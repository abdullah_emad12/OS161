# OS161 kernal

OS161 is a simple operating system written in C. It was built based on the [Harvard sys161](http://os161.eecs.harvard.edu)

## Getting Started

This section will guide you through the necessary steps to run the OS161 operating system.

#### Prerequisites

In order to run this OS you will need the OS161 Tool chains.According to you current operating system, You can follow the instruction of how to install the OS161 tool chain posted on the official [Hack the kernel](https://www.ops-class.org/asst/setup/) website.

#### Running the Terminal




## Kernel Design

This section contains the features that were added to the original Harvard sys161 including synchronization primitives, system calls and Memory Management unit.



#### Synchronization Primitives

1. Locks: allows only one thread to access a critical region. Blocks all the other threads until the lock has been released and wakes up the acquiring threads in FIFO order.
2. Condition Variables. 
3. Reader-Writer Locks.
4. Semaphores.


#### Processes 
