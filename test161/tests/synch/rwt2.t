---
name: "RW Lock Test 2"
description:
  Tests core reader-writer lock functionality by reading and writing shared
  writers are waiting.
tags: [synch, rwlocks, kleaks]
depends: [boot, semaphores, cvs]
sys161:
  cpus: 32
---
khu
rwt2
khu
