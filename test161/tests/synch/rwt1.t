---
name: "RW Lock Test 1"
description:
  Tests that reader-writer locks allow maximum read concurrency when no
  state.
tags: [synch, rwlocks, kleaks]
depends: [boot, semaphores]
sys161:
  cpus: 32
---
khu
rwt1
khu
