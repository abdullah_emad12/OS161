---
name: "RW Lock Test 3"
description:
  Tests writers starvation handling.
tags: [synch, rwlocks]
depends: [boot]
sys161:
  cpus: 16
---
rwt3
