/*
 * All the contents of this file are overwritten during automated
 * testing. Please consider this before changing anything in this file.
 */

#include <types.h>
#include <lib.h>
#include <clock.h>
#include <thread.h>
#include <synch.h>
#include <test.h>
#include <kern/test161.h>
#include <spinlock.h>


/*Constants*/
#define CREATELOOP 		16
#define NTHREADS   		32
#define NTHREADLOOPS	64	
/*Global variables*/
struct spinlock status_lock;
static bool test_status = TEST161_FAIL;

struct rwlock* rw_test = NULL;


static bool all_acquired = false; /*set to false while the all the readers has not acquired the lock yet*/
static volatile int reader_test = 12;
static volatile unsigned int writer_test = 0;
struct spinlock global_lock; /*used by threads to protect the update of global variables*/
static volatile unsigned int n_acquired_threads; /*number of the current acquiring threads*/ 
static struct semaphore *donesem = NULL; 
static volatile unsigned int total_acquired = 0; /*counter of the threads previously acquired the lock*/
static int current_thread_id = 0; /*the id of the current writer thread holding the lock*/
/*
 * sets the test status to false
 */
static bool failif(bool condition) {
	if (condition) {
		spinlock_acquire(&status_lock);
		test_status = TEST161_FAIL;
		spinlock_release(&status_lock);
	}
	return condition;
}


/**
 * Tests that the lock can be acquired by many readers 
 * Note: the thread assumes that only readers are trying to acquire to the lock
 */
static void readerthreadtest(void *junk, unsigned long num)
{
	(void)junk;
	(void)num;
	
	int acc = 0;
	unsigned int old_rw_n_acq = 0;
	
	
	spinlock_acquire(&global_lock);	
	// makes sure the the number of acquiring threads is incremented
	old_rw_n_acq = hashtable_length(rw_test->rw_acq_ht);
	rwlock_acquire_read(rw_test);
	KASSERT(hashtable_length(rw_test->rw_acq_ht) == old_rw_n_acq + 1);
	n_acquired_threads++;
	spinlock_release(&global_lock);
	
	
	kprintf_n("Thread number %lu acquired the lock...\n", num);
	
	KASSERT(rw_test->rw_current_mode == read);
	random_yielder(4);
	for(int i = 0; i < NTHREADLOOPS; i++)
	{
		acc += reader_test;
	}
	random_yielder(4);
	/*loops while other threads has not acquired the lock*/
	while(n_acquired_threads != NTHREADS && !all_acquired);
	
	all_acquired = true;
	
	/*Atomic actions*/
	spinlock_acquire(&global_lock);
	old_rw_n_acq = hashtable_length(rw_test->rw_acq_ht);
	rwlock_release_read(rw_test);
	KASSERT(hashtable_length(rw_test->rw_acq_ht) == old_rw_n_acq - 1);
	KASSERT(acc == reader_test * NTHREADLOOPS);
	old_rw_n_acq = n_acquired_threads--;
	KASSERT(n_acquired_threads == old_rw_n_acq - 1);
	spinlock_release(&global_lock);
	
	kprintf_n("Thread number %lu released the lock...\n", num);
	V(donesem);
}

/**
  * Makes sure that many readers can simultanuosly acquire the lock
  */
int rwtest(int nargs, char **args) {
	(void)nargs;
	(void)args;

	
	kprintf_n("Starting rwt1...\n");
	
	/*Attempts to create the lock multiple times*/
	for(int i = 0; i < CREATELOOP; i++)
	{
		kprintf_t(".");
		rw_test = rwlock_create("testlock");
		if(rw_test == NULL)
		{
			panic("rwt1: rwlock_create failed\n");
		}
		
		if(i != CREATELOOP - 1)
		{
			rwlock_destroy(rw_test);
		}
	}
	// semaphore that joins on the threads
	donesem = sem_create("donesem", 0);
	if (donesem == NULL) {
		panic("rwt1: sem_create failed\n");
	}
	
	
	spinlock_init(&status_lock);
	test_status = TEST161_SUCCESS;
	
	spinlock_init(&global_lock); 
	n_acquired_threads = 0;
	all_acquired = false;
	
	for(int i = 0; i < NTHREADS; i++)
	{
		kprintf_t(".");
		int result = thread_fork("synchtest", NULL, readerthreadtest, NULL, i);
		if (result) {
			panic("rwt1: thread_fork failed: %s\n", strerror(result));
		}
	}	
	
	for(int i = 0; i < NTHREADS; i++)
	{
		kprintf_t(".");
		P(donesem);
	}
	
	
	if(hashtable_length(rw_test->rw_acq_ht) != 0)
	{
		kprintf_n("rwt: Expects the number of acquiring threads to be 0 after the threads releases the lock but got %d\n", hashtable_length(rw_test->rw_acq_ht));
		test_status = TEST161_FAIL;
	}
	if(rw_test->rw_r_slots != 0)
	{
		kprintf_n("rwt: Expects the readers slots to be 0 after the solts were consumed but got %d\n", rw_test->rw_r_slots);
		test_status = TEST161_FAIL;
	}
	KASSERT(rw_test->rw_current_mode == none);
	/*frees the structures*/
	sem_destroy(donesem);
	rwlock_destroy(rw_test);
	donesem = NULL;
	rw_test = NULL;
	success(test_status, SECRET, "rwt1");


	return 0;
}




/*
 * Makes sure The lock can only be acquired by one writer
 * Note the thread assumes that only a write is trying to acquire the lock
 */

static void writerthreadtest(void *junk, unsigned long num)
{
	(void) junk;
	
	
	/*No need for a spinlock since only one thread can acquire the lock*/
	rwlock_acquire_write(rw_test);
	n_acquired_threads++;
	total_acquired++;
	
	writer_test += NTHREADS; 
	
	
	kprintf_n("Thread %lu acquired the lock\n", num);
	// the number of acquiring must be one
	if(n_acquired_threads != 1)
	{
		kprintf_n("rwt2: Expects only one writer thread to acquire the lock but %d threads acquired the lock\n", n_acquired_threads);
		goto fail;
	}
	random_yielder(4);
	if(hashtable_length(rw_test->rw_acq_ht) != 1)
	{
		kprintf_n("rwt2: The number of acquiring was not updated correctly. Expects 1 got %d\n", hashtable_length(rw_test->rw_acq_ht));
		goto fail;
	}
	if(rw_test->rw_current_mode != write)
	{
		kprintf_n("rwt2: The current mode is expected to be write got read instead\n");
		goto fail;
	}
	random_yielder(4);
	if(rw_test->rw_r_slots != RW_R_THRESH)
	{
		kprintf_n("rwt2: The number of the readers slot was not refilled correctly\n");
		goto fail;
	}
	random_yielder(4);
	if(writer_test != total_acquired * NTHREADS)
	{
		kprintf_n("rwt2: The test variable should be %d got %u instead\n", total_acquired * NTHREADS, writer_test);
		goto fail;
	}
	
	
	
	n_acquired_threads--;
	KASSERT(n_acquired_threads == 0);
	rwlock_release_write(rw_test);
	kprintf_n("Thread %lu released the lock\n", num);
	V(donesem);
	return;
	fail: 
	n_acquired_threads--;
	rwlock_release_write(rw_test);
	failif(true);
	V(donesem);
}
int rwtest2(int nargs, char **args) {
	(void)nargs;
	(void)args;
	kprintf_n("Starting rwt2...\n");
	rw_test = rwlock_create("testlock");
	if(rw_test == NULL)
	{
		panic("rwt1: rwlock_create failed\n");
	}
	
	// semaphore that joins on the threads
	donesem = sem_create("donesem", 0);
	if (donesem == NULL) {
		panic("rwt1: sem_create failed\n");
	}
		
	spinlock_init(&status_lock);
	test_status = TEST161_SUCCESS;
	
	spinlock_init(&global_lock); 
	n_acquired_threads = 0;
	all_acquired = false;

	/*runs some readers and wait for them
	 to make sure the writer lock is refilling the readers slots*/
	for(int i = 0; i < NTHREADS; i++)
	{
		kprintf_t(".");
		int result = thread_fork("synchtest", NULL, readerthreadtest, NULL, i);
		if (result) {
			panic("rwt2: thread_fork failed: %s\n", strerror(result));
		}
	}	
	for(int i = 0; i < NTHREADS; i++)
	{
		kprintf_t(".");
		P(donesem);
	}
	
	writer_test = 0;
	total_acquired = 0;
	// now creates the writers threads
	for(int i = 0; i < NTHREADS; i++)
	{
		kprintf_t(".");
		int result = thread_fork("synchtest", NULL, writerthreadtest, NULL, i);
		if (result) {
			panic("rwt2: thread_fork failed: %s\n", strerror(result));
		}
	}	
	for(int i = 0; i < NTHREADS; i++)
	{
		kprintf_t(".");
		P(donesem);
	}
	
	
	/*frees the structures*/
	sem_destroy(donesem);
	rwlock_destroy(rw_test);
	donesem = NULL;
	rw_test = NULL;
	success(test_status, SECRET, "rwt2");


	return 0;
}

/**
  * Acquires the lock then reads a global variable a NTHREADLOOPS times
  */

static void starvationreaderthreadtest(void *junk, unsigned long num)
{
	
	struct semaphore *read_done = (struct semaphore*) junk;
	KASSERT(rw_test != NULL);
	rwlock_acquire_read(rw_test);
	kprintf_n("Reader Thread %lu acquired the lock\n", num);
	spinlock_acquire(&global_lock);	
	n_acquired_threads++;
	current_thread_id = (int) num;
	spinlock_release(&global_lock);	
	
	if(rw_test->rw_current_mode != read)
	{
		kprintf_n("rwt3: The current mode is expected to be read\n");
		goto fail;
	}
	
	int old_writer_test = writer_test;
	int acc = 0;
	for(int i = 0; i < NTHREADLOOPS; i++)
	{
		acc += writer_test;
	}
	
	if(acc != old_writer_test * NTHREADLOOPS)
	{
		kprintf_n("rwt3: Expected a result of %d, Got %d\n", old_writer_test * NTHREADLOOPS, acc);
	}
	
	
	rwlock_release_read(rw_test);
	spinlock_acquire(&global_lock);	
	n_acquired_threads--;
	spinlock_release(&global_lock);	
	kprintf_n("Reader Thread %lu released the lock\n", num);
	V(read_done);
	V(donesem);
	
	return;
	fail: 
	spinlock_acquire(&global_lock);	
	n_acquired_threads++;
	spinlock_release(&global_lock);	
	rwlock_release_read(rw_test);
	failif(true);
	V(read_done);
	V(donesem);
}

/**
  * tries to acquire the lock and keeps accumalting random numbers to writer test random number of times 
  */
static void starvationwriterthreadtest(void *junk, unsigned long num)
{
	(void) junk;

	/*No need for a spinlock since only one thread can acquire the lock*/
	rwlock_acquire_write(rw_test);
	n_acquired_threads++;
	total_acquired++;
	current_thread_id = (int) num;
	
	
	kprintf_n("Writer Thread %lu acquired the lock\n", num);
	// the number of acquiring must be one
	if(n_acquired_threads != 1)
	{
		kprintf_n("rwt3: Expects only one writer thread to acquire the lock but %d threads acquired the lock\n", n_acquired_threads);
		goto fail;
	}
	random_yielder(4);
	if(hashtable_length(rw_test->rw_acq_ht) != 1)
	{
		kprintf_n("rwt3: The number of acquiring was not updated correctly. Expects 1 got %d\n", hashtable_length(rw_test->rw_acq_ht));
		goto fail;
	}
	if(rw_test->rw_current_mode != write)
	{
		kprintf_n("rwt3: The current mode is expected to be write got read instead\n");
		goto fail;
	}
	random_yielder(4);
	if(rw_test->rw_r_slots != RW_R_THRESH)
	{
		kprintf_n("rwt3: The number of the readers slot was not refilled correctly\n");
		goto fail;
	}
	random_yielder(4);

	for(int i = 0; i < NTHREADLOOPS; i++)
	{
		writer_test += (int) num;
	}
	

	KASSERT(current_thread_id == (int) num);
	
	n_acquired_threads--;
	KASSERT(n_acquired_threads == 0);
	rwlock_release_write(rw_test);
	kprintf_n("Writer Thread %lu released the lock\n", num);
	V(donesem);
	return;
	fail: 
	n_acquired_threads--;
	rwlock_release_write(rw_test);
	failif(true);
	V(donesem);
}


/**
  * Test starvation with infinite number of readers 
  */
int rwtest3(int nargs, char **args) {
	(void)nargs;
	(void)args;
	kprintf_n("Starting rwt3...\n");
	rw_test = rwlock_create("testlock");
	if(rw_test == NULL)
	{
		panic("rwt3: rwlock_create failed\n");
	}
	
	// semaphore that joins on the threads
	donesem = sem_create("donesem", 0);
	if (donesem == NULL) {
		panic("rwt3: sem_create failed\n");
	}
	
	struct semaphore *read_done = sem_create("donesem", 3); /*controls the number of forked reader threads*/ 
	if (read_done == NULL) {
		panic("rwt3: sem_create failed\n");
	}
		
	spinlock_init(&status_lock);
	spinlock_init(&global_lock);
	total_acquired = 0; /*will only count writers*/
	int writers_count = 0;
	n_acquired_threads = 0;
	writer_test = 1;
	test_status = TEST161_SUCCESS;
	int i;
	/*runs until N_Threads writer acquire the lock*/
	for(i = 0; total_acquired < NTHREADS; i++)
	{
		kprintf_t(".");
		if(i % 2 != 0 && writers_count < NTHREADS)
		{
			int result = thread_fork("synchtest", NULL, starvationwriterthreadtest, NULL, i);
			if (result) {
				panic("rwt3: thread_fork failed: %s\n", strerror(result));
			}
			writers_count++;
		}
		if(i % 2 == 0 || writers_count >= NTHREADS)
		{
			int result = thread_fork("synchtest", NULL, starvationreaderthreadtest, read_done, i);
			if (result) {
				panic("rwt3: thread_fork failed: %s\n", strerror(result));
			}
			P(read_done);
		}
		
	}

	for(int j = 0; j < i + 1; j++)
	{
		kprintf_t(".");
		P(donesem);
	}
	
	
	/*frees the structures*/
	sem_destroy(donesem);
	rwlock_destroy(rw_test);
	donesem = NULL;
	rw_test = NULL;
	success(test_status, SECRET, "rwt3");



	return 0;
}

/**
  * should not attempt to release the lock if you don't already hold it
  */
int rwtest4(int nargs, char **args) {
	(void)nargs;
	(void)args;

	(void)nargs;
	(void)args;

	kprintf_n("Starting rwt4...\n");
	kprintf_n("(This test panics on success!)\n");

	rw_test = rwlock_create("testlock");
	if(rw_test == NULL)
	{
		panic("rwt4: rwlock_create failed\n");
	}

	secprintf(SECRET, "Should panic...", "rwt4");
	rwlock_release_write(rw_test);

	/* Should not get here on success. */

	success(TEST161_FAIL, SECRET, "rwt4");

	/* Don't do anything that could panic. */

	rw_test = NULL;
	return 0;

	return 0;
}
/**
  * shouldn't destroy the lock while threads are still holding it
  */
int rwtest5(int nargs, char **args) {
	(void)nargs;
	(void)args;

	kprintf_n("Starting rwt5...\n");
	kprintf_n("(This test panics on success!)\n");

	rw_test = rwlock_create("testlock");
	if(rw_test == NULL)
	{
		panic("rwt5: rwlock_create failed\n");
	}

	secprintf(SECRET, "Should panic...", "rwt5");
	rwlock_acquire_read(rw_test);
	rwlock_destroy(rw_test);

	/* Should not get here on success. */

	success(TEST161_FAIL, SECRET, "rwt5");

	/* Don't do anything that could panic. */

	rw_test = NULL;
	return 0;
}
