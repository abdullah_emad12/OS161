/*
 * Copyright (c) 2000, 2001, 2002, 2003, 2004, 2005, 2008, 2009
 *	The President and Fellows of Harvard College.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/**
  * This file contains tests for the file system calls
  */

#include <types.h>
#include <lib.h>
#include <syscall.h>
#include <proc.h>
#include <kern/seek.h>
#include <test.h>
#include <vfs.h>
#include <kern/test161.h>
#include <kern/fcntl.h>
#include <kern/stat.h>
#include <current.h>
#include <kern/errno.h>
#include <filehandle.h>

/*prototypes*/
static bool check_file_handle(struct filehandle* fh, const char* filename, bool expected_r, bool expected_w);

static bool test_status = TEST161_FAIL;

int sysopentest1(int nargs, char **args)
{
	(void)nargs;
	(void)args;
	kprintf_n("Starting sysopt1...\n");
	test_status = TEST161_SUCCESS;
	
	/*create a process*/
	
	struct proc* proc;
	proc = proc_create_runprogram("test");
	struct proc* old_proc = curproc;
	curproc = proc;		
	/*make sure that there exists a file handle for stdin, stdout, stderr*/
	struct filehandle* fh = cbt_search(proc->p_cbt_fds, 0);
	char* filename1 = (char*)"con\0\0";
	filename1[3] = ':';
	if(!check_file_handle(fh, filename1, true, false))
	{
		test_status = TEST161_FAIL;
	}
	for(int i = 0; i < 2; i++)
	{
		fh = cbt_search(proc->p_cbt_fds, i+1);
		char* filename2 = (char*)"con\0\0\0\0";
		filename2[3] = ':';
		if(!check_file_handle(fh, filename2, false, true))
		{
			test_status = TEST161_FAIL;
		}
	}
	

	/*attempts to open a file for reading*/
	const char* file = "testfile.txt";
	int fd;
	int err = sys_open(file, O_RDONLY|O_CREAT, &fd);
	if(err)
	{			
		kprintf_n("sysopt1: failed to open the file\n");
		test_status = TEST161_FAIL;
	}
	if(fd != 3)
	{
		kprintf_n("sysopt1: Expecting the fd to be 3, instead got %d\n", fd);
		test_status = TEST161_FAIL;
	}
	struct filehandle* new_file = cbt_search(proc->p_cbt_fds, 3);
	if(!check_file_handle(new_file, file, true, false))
	{
		test_status = TEST161_FAIL;
	}
	
	
	
	const char* file1 = "testfile.txt";
	err = sys_open(file1, O_WRONLY, &fd);
	if(err)
	{			
		kprintf_n("sysopt1: failed to open the file\n");
		test_status = TEST161_FAIL;
	}
	if(fd != 4)
	{
		kprintf_n("sysopt1: Expecting the fd to be 4 instead got %d\n", fd);
		test_status = TEST161_FAIL;
	}
	
	new_file = cbt_search(proc->p_cbt_fds, 4);
	if(!check_file_handle(new_file, file, false, true))
	{
		test_status = TEST161_FAIL;
	}
	
	
	const char* file3 = "testfile.txt";
	err = sys_open(file3, O_RDWR ,&fd);
	if(err)
	{			
		kprintf_n("sysopt1: failed to open the file\n");
		test_status = TEST161_FAIL;
	}
	if(fd != 5)
	{
		kprintf_n("sysopt1: Expecting the fd to be 5 instead got %d\n", fd);
		test_status = TEST161_FAIL;
	}
	
	new_file = cbt_search(proc->p_cbt_fds, 5);
	if(!check_file_handle(new_file, file, true, true))
	{
		test_status = TEST161_FAIL;
	}
	
	
	success(test_status, SECRET, "sysopt1");
	proc_destroy(proc);
	curproc = old_proc;
	return 0;
	
}


int sysopentest2(int nargs, char **args)
{
	(void)nargs;
	(void)args;
	kprintf_n("Starting sysopt2...\n");
	test_status = TEST161_SUCCESS;
	
	/*create a process*/
	
	struct proc* proc;
	proc = proc_create_runprogram("test");
	struct proc* old_proc = curproc;
	curproc = proc;		
	

	/*attempts to open a file that does not exist for reading*/
	const char* file = "testfile1111.txt";
	int fd;
	int err = sys_open(file, O_RDONLY, &fd);
	if(err == 0)
	{		
		kprintf("%d  %d\n\n", fd, err);
		kprintf_n("sysopt2: It should have failed to open the file\n");
		test_status = TEST161_FAIL;
	}
	
	struct filehandle* new_file = cbt_search(proc->p_cbt_fds, 3);
	if(new_file != NULL)
	{
		kprintf_n("sysopt2: A file handle was found, expected none error code: %d\n", err);
		test_status = TEST161_FAIL;
	}
	
		
	
	success(test_status, SECRET, "sysopt1");
	proc_destroy(proc);
	curproc = old_proc;
	return 0;
	
}





int syslseektest1(int nargs, char **args)
{
	(void)nargs;
	(void)args;

	kprintf_n("Starting syslseekt1...\n");
	test_status = TEST161_SUCCESS;
		
	/*creates a new process*/	
	struct proc* proc;
	proc = proc_create_runprogram("test");
	struct proc* old_proc = curproc;
	curproc = proc;	


	/*attempts to open a file that does not exist for writing*/
	const char* file = "testfile11.txt";
	int fd;
	int err = sys_open(file, O_WRONLY|O_CREAT , &fd);

	// try seeking 
	int32_t ret;
	int32_t ret2;
	err = sys_lseek(fd, 30, SEEK_SET, &ret, &ret2);
	if(err != 0)
	{
		kprintf_n("syslseekt1: Couldn't seek and returned error: %d\n", err);
		test_status = TEST161_FAIL;	
	}
	
	struct filehandle* fh = cbt_search(proc->p_cbt_fds, fd);
	KASSERT(fh != NULL);
	
	if(fh->fh_offset != 30)
	{
		kprintf_n("syslseekt1: Expected the offset value to be 30 instead got: %d\n", (int)fh->fh_offset);
		test_status = TEST161_FAIL;	
	}
	
	/*destroys the process*/
	success(test_status, SECRET, "syslseekt1");
	proc_destroy(proc);
	curproc = old_proc;
	return 0;
} 



int syslseektest2(int nargs, char **args)
{
	(void)nargs;
	(void)args;

	kprintf_n("Starting syslseekt2...\n");
	test_status = TEST161_SUCCESS;
		
	/*creates a new process*/	
	struct proc* proc;
	proc = proc_create_runprogram("test");
	struct proc* old_proc = curproc;
	curproc = proc;	


	/*attempts to open a file that does not exist for writing*/
	const char* file = "testfile11.txt";
	int fd;
	int err = sys_open(file, O_WRONLY|O_CREAT , &fd);

	// try seeking with negative value
	int32_t ret;
	int32_t ret2;
	err = sys_lseek(fd, -30, SEEK_SET, &ret, &ret2);
	if(err != EINVAL)
	{
		kprintf_n("syslseekt2: Expected EINVAL error and got error: %d instead\n", err);
		test_status = TEST161_FAIL;	
	}
	
	struct filehandle* fh = cbt_search(proc->p_cbt_fds, fd);
	KASSERT(fh != NULL);
	
	if(fh->fh_offset != 0)
	{
		kprintf_n("syslseekt2: Expected the offset value to be 0 instead got: %d\n", (int)fh->fh_offset);
		test_status = TEST161_FAIL;	
	}
	
	/*destroys the process*/
	success(test_status, SECRET, "syslseekt2");
	proc_destroy(proc);
	curproc = old_proc;
	return 0;
} 

int syslseektest3(int nargs, char **args)
{
	(void)nargs;
	(void)args;

	kprintf_n("Starting syslseekt3...\n");
	test_status = TEST161_SUCCESS;
		
	/*creates a new process*/	
	struct proc* proc;
	proc = proc_create_runprogram("test");
	struct proc* old_proc = curproc;
	curproc = proc;	


	// try seeking with on console device
	int32_t ret;
	int32_t ret2;
	int err = sys_lseek(0, -30, SEEK_SET, &ret, &ret2);
	if(err != ESPIPE)
	{
		kprintf_n("syslseekt1: Expected ESPIPE error and got error: %d instead\n", err);
		test_status = TEST161_FAIL;	
	}
	
	
	/*destroys the process*/
	success(test_status, SECRET, "syslseekt3");
	proc_destroy(proc);
	curproc = old_proc;
	return 0;
} 


int syslseektest4(int nargs, char **args)
{
	(void)nargs;
	(void)args;

	kprintf_n("Starting syslseekt2...\n");
	test_status = TEST161_SUCCESS;
		
	/*creates a new process*/	
	struct proc* proc;
	proc = proc_create_runprogram("test");
	struct proc* old_proc = curproc;
	curproc = proc;	


	/*attempts to open a file that does not exist for writing*/
	const char* file = "testfile11.txt";
	int fd;
	int err = sys_open(file, O_WRONLY|O_CREAT , &fd);

	// try seeking with invalid whence
	int32_t ret;
	int32_t ret2;
	err = sys_lseek(fd, 30, 254, &ret, &ret2);
	if(err != EINVAL)
	{
		kprintf_n("syslseekt1: Expected EINVAL error and got error: %d instead\n", err);
		test_status = TEST161_FAIL;	
	}
	
	struct filehandle* fh = cbt_search(proc->p_cbt_fds, fd);
	KASSERT(fh != NULL);
	
	if(fh->fh_offset != 0)
	{
		kprintf_n("syslseekt1: Expected the offset value to be 0 instead got: %d\n", (int)fh->fh_offset);
		test_status = TEST161_FAIL;	
	}
	
	/*destroys the process*/
	success(test_status, SECRET, "syslseekt4");
	proc_destroy(proc);
	curproc = old_proc;
	return 0;
} 


int sysclosetest1(int nargs, char **args)
{
	(void)nargs;
	(void)args;
	kprintf_n("Starting syscloset1...\n");
	test_status = TEST161_SUCCESS;
		
	/*creates a new process*/	
	struct proc* proc;
	proc = proc_create_runprogram("test");
	struct proc* old_proc = curproc;
	curproc = proc;	


	/*attempts to open a file that does not exist for writing*/
	const char* file = "testfile11.txt";
	int fd;
	int err = sys_open(file, O_WRONLY|O_CREAT , &fd);

	// try closing 
	int32_t ret;
	err = sys_close(fd, &ret);
	if(err != 0)
	{
		kprintf_n("syscloset1: Couldn't close and returned error: %d\n", err);
		test_status = TEST161_FAIL;	
	}
	
	struct filehandle* fh = cbt_search(proc->p_cbt_fds, fd);
	if(fh != NULL)
	{
		kprintf_n("syscloset1: The file handle was not removed from the tree \n");
		test_status = TEST161_FAIL;	
	}
	/*destroys the process*/
	success(test_status, SECRET, "syscloset1");
	proc_destroy(proc);
	curproc = old_proc;
	return 0;

}

int sysclosetest2(int nargs, char **args)
{
	(void)nargs;
	(void)args;
	kprintf_n("Starting syscloset2...\n");
	test_status = TEST161_SUCCESS;
		
	/*creates a new process*/	
	struct proc* proc;
	proc = proc_create_runprogram("test");
	struct proc* old_proc = curproc;
	curproc = proc;	


	int fd = 5;


	// try closing a not valid fd
	int32_t ret;
	int err = sys_close(fd, &ret);
	if(err != EBADF)
	{
		kprintf_n("syscloset1: Expected error code \"EBADF\", got error: %d\n", err);
		test_status = TEST161_FAIL;	
	}
	
	struct filehandle* fh = cbt_search(proc->p_cbt_fds, fd);
	if(fh != NULL)
	{
		kprintf_n("syscloset1: The file handle was not removed from the tree \n");
		test_status = TEST161_FAIL;	
	}
	/*destroys the process*/
	success(test_status, SECRET, "syscloset2");
	proc_destroy(proc);
	curproc = old_proc;
	return 0;

}


int sysdup2test1(int nargs, char **args)
{
	(void)nargs;
	(void)args;
	kprintf_n("Starting sysdup2t1...\n");
	test_status = TEST161_SUCCESS;
		
	/*creates a new process*/	
	struct proc* proc;
	proc = proc_create_runprogram("test");
	struct proc* old_proc = curproc;
	curproc = proc;	


	/*attempts to open a file that does not exist for writing*/
	const char* file = "testfile11.txt";
	int fd;
	int err = sys_open(file, O_WRONLY|O_CREAT , &fd);
	
	int newfd;
	err = sys_dup2(fd, 344, &newfd);
	
	if(err)
	{
		kprintf_n("sysdup2t1: Expected error code 0, got error: %d\n", err);
		test_status = TEST161_FAIL;	
	}
	
	
	struct filehandle* fh = cbt_search(proc->p_cbt_fds, newfd);
	if(fh == NULL)
	{
		kprintf_n("sysdup2t1: Couldn't find the clone \n");
		test_status = TEST161_FAIL;	
		goto end;
	}

	struct filehandle* fh_original = cbt_search(proc->p_cbt_fds, fd);
	if(fh_original->fh_vnode != fh->fh_vnode)
	{
		kprintf_n("sysdup2t1: the vnodes are not the same \n");
		test_status = TEST161_FAIL;	
		goto end;
	}
	else if(fh_original->fh_offset != fh->fh_offset)
	{
		kprintf_n("sysdup2t1: the offsets are not the same \n");
		test_status = TEST161_FAIL;
		goto end;	
	}
	else if(fh_original->fh_read != fh->fh_read)
	{
		kprintf_n("sysdup2t1: the read are not the same \n");
		test_status = TEST161_FAIL;
		goto end;	
	}
	else if(fh_original->fh_write != fh->fh_write)
	{
		kprintf_n("sysdup2t1: the write are not the same \n");
		test_status = TEST161_FAIL;	
		goto end;
	}
	else if((fh_original->fh_nholders != fh->fh_nholders) && *(fh->fh_nholders) != 2)
	{
		kprintf_n("sysdup2t1: the number of holders is not the same \n");
		test_status = TEST161_FAIL;	
		goto end;
	}


	ll_node_t* node = ll_front(proc->p_free_fds);
	if(node == NULL)
	{
		kprintf_n("sysdup2t1: the chunk was was not added successfully.  \n");
		test_status = TEST161_FAIL;
		goto end;
	}
	struct fd_chunk* ck = (struct fd_chunk*) node->object;
	if(ck->ck_lower != (unsigned) fd + 1)
	{
		kprintf_n("sysdup2t1: the lower bound is expected to be %d, got %d instead \n", fd+1, ck->ck_lower);
		test_status = TEST161_FAIL;
	}
	else if (ck->ck_upper != (unsigned) newfd - 1)
	{
		kprintf_n("sysdup2t1: the upper bound is expected to be %d, got %d instead \n", newfd-1, ck->ck_upper);
		test_status = TEST161_FAIL;
	}

	/*destroys the process*/
 end:	sys_close(fd, &err);
		success(test_status, SECRET, "sysdup2t1");
		proc_destroy(proc);
		curproc = old_proc;
		return 0;
}


int sysdup2test2(int nargs, char **args)
{
	(void)nargs;
	(void)args;
	kprintf_n("Starting sysdup2t2...\n");
	test_status = TEST161_SUCCESS;
		
	/*creates a new process*/	
	struct proc* proc;
	proc = proc_create_runprogram("test");
	struct proc* old_proc = curproc;
	curproc = proc;	


	/*attempts to open a file that does not exist for writing*/
	const char* file = "testfile11.txt";
	int fd;
	int err = sys_open(file, O_WRONLY|O_CREAT , &fd);

	const char* file1 = "testfile12.txt";
	int fd1;
	err = sys_open(file1, O_WRONLY|O_CREAT , &fd1);

	
	int newfd;
	err = sys_dup2(fd, fd1, &newfd);
	
	if(err)
	{
		kprintf_n("sysdup2t2: Expected error code 0, got error: %d\n", err);
		test_status = TEST161_FAIL;	
	}
	
	
	struct filehandle* fh = cbt_search(proc->p_cbt_fds, newfd);
	if(fh == NULL)
	{
		kprintf_n("sysdup2t2: Couldn't find the clone \n");
		test_status = TEST161_FAIL;	
		goto end;
	}

	struct filehandle* fh_original = cbt_search(proc->p_cbt_fds, fd);
	if(fh_original->fh_vnode != fh->fh_vnode)
	{
		kprintf_n("sysdup2t2: the vnodes are not the same \n");
		test_status = TEST161_FAIL;	
		goto end;
	}
	else if(fh_original->fh_offset != fh->fh_offset)
	{
		kprintf_n("sysdup2t2: the offsets are not the same \n");
		test_status = TEST161_FAIL;
		goto end;	
	}
	else if(fh_original->fh_read != fh->fh_read)
	{
		kprintf_n("sysdup2t2: the read are not the same \n");
		test_status = TEST161_FAIL;
		goto end;	
	}
	else if(fh_original->fh_write != fh->fh_write)
	{
		kprintf_n("sysdup2t2: the write are not the same \n");
		test_status = TEST161_FAIL;	
		goto end;
	}
	else if((fh_original->fh_nholders != fh->fh_nholders) && *(fh->fh_nholders) != 2)
	{
		kprintf_n("sysdup2t2: the number of holders is not the same \n");
		test_status = TEST161_FAIL;	
		goto end;
	}


	ll_node_t* node = ll_front(proc->p_free_fds);
	if(node != NULL)
	{
		kprintf_n("sysdup2t2: Expected the free list of fds to be empty.  \n");
		test_status = TEST161_FAIL;
		goto end;
	}


	/*destroys the process*/
 end:	sys_close(fd, &err);
		success(test_status, SECRET, "sysdup2t2");
		proc_destroy(proc);
		curproc = old_proc;
		return 0;
}

/******************
 *                * 
 *     helpers    *
 *                *
 ******************/
 
 static bool check_file_handle(struct filehandle* fh, const char* filename, bool expected_r, bool expected_w)
 {
 	if(fh == NULL)
 	{
 		return false;
 	}
 	struct vnode* expected_vnode = NULL;
 	int err = vfs_lookup((char*)filename, &(expected_vnode));
 	if(err)
 	{
 		kprintf_n("sysopt1: The lookup function return error of %d for file %s\n", err, filename);
 		return false;
 	}
 	if(fh->fh_vnode != expected_vnode)
 	{
 		kprintf_n("sysopt1: The vnodes do not match\n");
 		return false;
 	}
 	if(fh->fh_read != expected_r)
 	{
 		kprintf_n("sysopt1: The mode is not setup correctly\n");
 		return false;
 	}
 	if(fh->fh_write != expected_w)
 	{
 		kprintf_n("sysopt1: The mode is not setup correctly\n");
 		return false;
 	}
 	return true;
 }
