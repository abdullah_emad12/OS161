/*
 * Copyright (c) 2000, 2001, 2002, 2003, 2004, 2005, 2008, 2009
 *	The President and Fellows of Harvard College.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */




#include <types.h>
#include <lib.h>
#include <clock.h>
#include <thread.h>
#include <synch.h>
#include <test.h>
#include <kern/test161.h>
#include <spinlock.h>
#include <proc.h>
#include <vnode.h>
#include <vfs.h>


#define MAX_ITERATIONS 500
#define MAX_PROCESSES 30

static bool test_status = TEST161_FAIL;


 static bool checkUnique(struct proc* procs[], int length);

/*
 * Test for PID allocation functionality
 * Makes sure it never runs of PIDs
 */
int pidalloctest(int nargs, char **args)
{
	(void)nargs;
	(void)args;
	kprintf_n("Starting proct1...\n");
	
	kprintf_n("If this hangs or panics, it's broken: \n");
	
	kprintf_n("This may take a few seconds...\n");
	for(int i = 0; i < MAX_ITERATIONS; i++)
	{
		
		struct proc* active_procs[MAX_PROCESSES];
		for(int j = 0; j < MAX_PROCESSES; j++)
		{
			active_procs[j] = proc_create_runprogram("test");
		}
		
		for(int j = 0; j < MAX_PROCESSES; j++)
		{
			if(active_procs[j] != NULL)
				proc_destroy(active_procs[j]);
		}
	}
	
	test_status = TEST161_SUCCESS;
	
	kprintf_t("\n");
	success(test_status, SECRET, "proct1");
	return 0;
}

/*
 * Tests the uniqueness of pids
 * should panic on success
 */
int pidalloctest2(int nargs, char **args)
{
	(void)nargs;
	(void)args;
	kprintf_n("Starting proct2...\n");
	
	kprintf_n("If this hangs or panics, it's broken: \n");
	
	kprintf_n("This may take a few seconds...\n");
	test_status = TEST161_SUCCESS;
	
	for(int i = 0; i < MAX_ITERATIONS; i++)
	{
		struct proc* active_procs[MAX_PROCESSES];
		for(int j = 0; j < MAX_PROCESSES; j++)
		{
			active_procs[j] = proc_create_runprogram("test");
		}
		bool unique = checkUnique(active_procs, MAX_PROCESSES);
		if(!unique)
		{
			test_status = TEST161_FAIL;
			break;
		}
		for(int j = 0; j < MAX_PROCESSES; j++)
		{
			proc_destroy(active_procs[j]);
		}
	}
	

	
	kprintf_t("\n");
	success(test_status, SECRET, "proct2");
	return 0;
}



/**
  * listofprocess, int -> bool
  * returns true if the array of processes have no two processes with the same pid
  */

 static bool checkUnique(struct proc* procs[], int length)
 {
 	for(int i = 0; i < length; i++)
 	{
 		struct proc* proc = procs[i];
 		for(int j = i + 1; j < length; j++)
 		{
 			if(proc->p_id == procs[j]->p_id)
 			{
 				return false;
 			}
 		}
 	
 	} 
 	return true;
 }
  
