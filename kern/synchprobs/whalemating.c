/*
 * Copyright (c) 2001, 2002, 2009
 *	The President and Fellows of Harvard College.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/*
 * Driver code is in kern/tests/synchprobs.c We will
 * replace that file. This file is yours to modify as you see fit.
 *
 * You should implement your solution to the whalemating problem below.
 */

#include <types.h>
#include <lib.h>
#include <thread.h>
#include <test.h>
#include <synch.h>
/*Global locking primitives*/
struct semaphore* sem_male = NULL; 
struct semaphore* sem_female = NULL; 
struct semaphore* sem_male_female = NULL; /*up by male, down by female*/
struct semaphore* sem_female_male = NULL; /*up by female, down by male*/
struct cv* cv_match_maker = NULL; 
struct lock* lk_match_maker = NULL;

/*
 * Called by the driver during initialization.
 */

void whalemating_init() 
{
	/*they should be NULL at the beginning*/
	KASSERT(sem_male == NULL);
	KASSERT(sem_female == NULL);
	KASSERT(sem_male_female == NULL);
	KASSERT(sem_female_male == NULL);
	KASSERT(cv_match_maker == NULL);
	
	/*intialzes global semphores*/
	sem_male = sem_create("male", 0); 
	if(sem_male == NULL)
	{
		panic("sp1: sem_create failed\n");
	}
	
	
	sem_female = sem_create("female", 0);
	if(sem_female == NULL)
	{
		sem_destroy(sem_male);
		panic("sp1: sem_create failed\n");
	}
	
	sem_male_female = sem_create("malefemale", 0);
	if(sem_male_female == NULL)
	{
		sem_destroy(sem_male);
		sem_destroy(sem_female);
		panic("sp1: sem_create failed\n");
	}
	
	
	sem_female_male = sem_create("femalemale", 0);
	if(sem_female_male == NULL)
	{
		sem_destroy(sem_male);
		sem_destroy(sem_female);
		sem_destroy(sem_male_female);
		panic("sp1: sem_create failed\n");		
	}
	
	cv_match_maker = cv_create("matchmaker");
	if(cv_match_maker == NULL)
	{
		sem_destroy(sem_male);
		sem_destroy(sem_female);
		sem_destroy(sem_male_female);
		sem_destroy(sem_female_male);
		panic("sp1: sem_create failed\n");
	}
	
	lk_match_maker = lock_create("lkmatchmaker");
	if(lk_match_maker == NULL)
	{
		sem_destroy(sem_male);
		sem_destroy(sem_female);
		sem_destroy(sem_male_female);
		sem_destroy(sem_female_male);
		cv_destroy(cv_match_maker);
		panic("sp1: sem_create failed\n");
	}
	return;
}

/*
 * Called by the driver during teardown.
 */

void whalemating_cleanup() 
{
	/*whalemating init must be called first*/
	KASSERT(sem_male != NULL);
	KASSERT(sem_female != NULL);
	KASSERT(sem_male_female != NULL);
	KASSERT(sem_female_male != NULL);
	KASSERT(cv_match_maker != NULL);

	/*destroys the lock*/
	sem_destroy(sem_male);
	sem_destroy(sem_female);
	sem_destroy(sem_male_female);
	sem_destroy(sem_female_male);
	cv_destroy(cv_match_maker);
	lock_destroy(lk_match_maker);
	
	/*sets the locks to NULL*/
	sem_male = NULL;
	sem_female = NULL;
	sem_male_female = NULL;
	sem_female_male = NULL;
	cv_match_maker = NULL;
	lk_match_maker = NULL;
	
	
	return;
}

void male(uint32_t index)
{
	(void)index;
	male_start(index);
	
	/*waits for a matcher*/
	P(sem_male);
	/*sginals to a female*/
	V(sem_male_female);
	/*waits for a female to signal to it*/
	P(sem_female_male);
	
	
	
 	male_end(index);
	return;
}

void female(uint32_t index)
{
	(void)index;
	female_start(index);
	
	/*waits for matcher*/
	P(sem_female);
	/*Signals for a male*/
	V(sem_female_male);
	/*waits for the male to signal to it*/
	P(sem_male_female);
	
	/*signals to the match maker*/
	lock_acquire(lk_match_maker);
	
	cv_signal(cv_match_maker, lk_match_maker);
	
	lock_release(lk_match_maker);
	female_end(index); 
	return;
}

void
matchmaker(uint32_t index)
{
	(void)index;
	matchmaker_start(index);
	
	V(sem_male);
	V(sem_female);
	
	/*waits for signal to indicate that the female and male are both in*/
	lock_acquire(lk_match_maker);
	cv_wait(cv_match_maker, lk_match_maker);
	lock_release(lk_match_maker);

	
	
	matchmaker_end(index);
	return;
}
