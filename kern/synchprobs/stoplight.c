/*
 * Copyright (c) 2001, 2002, 2009
 *	The President and Fellows of Harvard College.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/*
 * Driver code is in kern/tests/synchprobs.c We will replace that file. This
 * file is yours to modify as you see fit.
 *
 * You should implement your solution to the stoplight problem below. The
 * quadrant and direction mappings for reference: (although the problem is, of
 * course, stable under rotation)
 *
 *   |0 |
 * -     --
 *    01  1
 * 3  32
 * --    --
 *   | 2|
 *
 * As way to think about it, assuming cars drive on the right: a car entering
 * the intersection from direction X will enter intersection quadrant X first.
 * The semantics of the problem are that once a car enters any quadrant it has
 * to be somewhere in the intersection until it call leaveIntersection(),
 * which it should call while in the final quadrant.
 *
 * As an example, let's say a car approaches the intersection and needs to
 * pass through quadrants 0, 3 and 2. Once you call inQuadrant(0), the car is
 * considered in quadrant 0 until you call inQuadrant(3). After you call
 * inQuadrant(2), the car is considered in quadrant 2 until you call
 * leaveIntersection().
 *
 * You will probably want to write some helper functions to assist with the
 * mappings. Modular arithmetic can help, e.g. a car passing straight through
 * the intersection entering from direction X will leave to direction (X + 2)
 * % 4 and pass through quadrants X and (X + 3) % 4.  Boo-yah.
 *
 * Your solutions below should call the inQuadrant() and leaveIntersection()
 * functions in synchprobs.c to record their progress.
 */

#include <types.h>
#include <lib.h>
#include <thread.h>
#include <test.h>
#include <synch.h>

/*Constants*/
#define CARSLIMIT 3

/*Global locks*/
struct semaphore* sem_intersection = NULL;
struct lock* lk_quad0 = NULL;
struct lock* lk_quad1 = NULL;
struct lock* lk_quad2 = NULL;
struct lock* lk_quad3 = NULL;


/*Helpers prototypes*/
void acquire_ps_lock(uint32_t ps);
void release_ps_lock(uint32_t ps);

/*
 * Called by the driver during initialization.
 */

void stoplight_init() 
{
	KASSERT(sem_intersection == NULL);
	KASSERT(lk_quad0 == NULL);
	KASSERT(lk_quad1 == NULL);
	KASSERT(lk_quad2 == NULL);
	KASSERT(lk_quad3 == NULL);
	
	
	sem_intersection = sem_create("sem_intersection", CARSLIMIT);
	if(sem_intersection == NULL)
	{
		panic("sp2: sem_create failed\n");
	}
	
	lk_quad0 = lock_create("quad0");
	if(lk_quad0 == NULL)
	{
		sem_destroy(sem_intersection);
		panic("sp2: lock_create failed\n");
	}
	
	lk_quad1 = lock_create("quad1");
	if(lk_quad1 == NULL)
	{
		sem_destroy(sem_intersection);
		lock_destroy(lk_quad0);
		panic("sp2: lock_create failed\n");
	}
	
	lk_quad2 = lock_create("quad2");
	if(lk_quad2 == NULL)
	{
		sem_destroy(sem_intersection);
		lock_destroy(lk_quad0);
		lock_destroy(lk_quad1);
		panic("sp2: lock_create failed\n");
	}
	
	lk_quad3 = lock_create("quad3");
	if(lk_quad3 == NULL)
	{
		sem_destroy(sem_intersection);
		lock_destroy(lk_quad0);
		lock_destroy(lk_quad1);
		lock_destroy(lk_quad2);		
		panic("sp2: lock_create failed\n");
	}
	return;
}

/*
 * Called by the driver during teardown.
 */

void stoplight_cleanup() 
{

	/*Some assertions*/
	KASSERT(sem_intersection != NULL);
	KASSERT(lk_quad0 != NULL);
	KASSERT(lk_quad1 != NULL);
	KASSERT(lk_quad2 != NULL);
	KASSERT(lk_quad3 != NULL);
	
	/*Destroys all the lock*/
	sem_destroy(sem_intersection);
	lock_destroy(lk_quad0);
	lock_destroy(lk_quad1);
	lock_destroy(lk_quad2);
	lock_destroy(lk_quad3);
	
	
	/*sets all the lock to null*/
	sem_intersection = NULL;
	lk_quad0 = NULL;
	lk_quad1 = NULL;
	lk_quad2 = NULL;
	lk_quad3 = NULL;

	
	return;
}

void turnright(uint32_t direction, uint32_t index)
{
	/*path vector*/
	uint32_t pv[1];
	pv[0] = direction;
	/*Attempts to enter the intersection*/
	P(sem_intersection);
	for(int i = 0; i < 1; i++)
	{
		acquire_ps_lock(pv[i]);
		inQuadrant(pv[i], index);
		if(i > 0)
		{
			release_ps_lock(pv[i - 1]);
		}
	}
	/*Leaves the intersection*/
	leaveIntersection(index);
	release_ps_lock(pv[0]);
	V(sem_intersection);

	return;
}
void gostraight(uint32_t direction, uint32_t index)
{
	/*path vector*/
	uint32_t pv[2];
	pv[0] = direction;
	pv[1] = (direction + 3) % 4;
	
	/*Attempts to enter the intersection*/
	P(sem_intersection);
	for(int i = 0; i < 2; i++)
	{
		acquire_ps_lock(pv[i]);
		inQuadrant(pv[i], index);
		if(i > 0)
		{
			release_ps_lock(pv[i - 1]);
		}
	}
	/*Leaves the intersection*/
	leaveIntersection(index);
	release_ps_lock(pv[1]);
	V(sem_intersection);


	return;
}
void turnleft(uint32_t direction, uint32_t index)
{
	/*path vector*/
	uint32_t pv[3];
	pv[0] = direction;
	pv[1] = (direction + 3) % 4;
	pv[2] = (direction + 2) % 4;

	
	/*Attempts to enter the intersection*/
	P(sem_intersection);
	for(int i = 0; i < 3; i++)
	{
		acquire_ps_lock(pv[i]);
		inQuadrant(pv[i], index);
		if(i > 0)
		{
			release_ps_lock(pv[i - 1]);
		}
	}
	/*Leaves the intersection*/
	leaveIntersection(index);
	release_ps_lock(pv[2]);
	V(sem_intersection);


	
	
	return;
}

/*******************************************
*										   *
*				Helpers					   *
*										   *
********************************************/

/**
  * uint32_t -> void
  * acquires the correct lock according to the path scalar
  *
  * uint32_t ps: the path scalar of the next hop
  *
  */
void acquire_ps_lock(uint32_t ps)
{
	KASSERT(ps <= 3);
	
	
	switch(ps)
	{
		case 0: 
		{
			lock_acquire(lk_quad0);
			break;
		}
		case 1: 
		{
			lock_acquire(lk_quad1);
			break;
		}
		case 2: 
		{
			lock_acquire(lk_quad2);
			break;
		}
		case 3: 
		{
			lock_acquire(lk_quad3);
			break;
		}
	
	}
}  
 
 
/**
  * uint32_t -> void
  * releases the correct lock according to the path scalar
  *
  * uint32_t ps: the path scalar of the next hop
  *
  */
void release_ps_lock(uint32_t ps)
{
	KASSERT(ps <= 3);
	
	
	switch(ps)
	{
		case 0: 
		{
			lock_release(lk_quad0);
			break;
		}
		case 1: 
		{
			lock_release(lk_quad1);
			break;
		}
		case 2: 
		{
			lock_release(lk_quad2);
			break;
		}
		case 3: 
		{
			lock_release(lk_quad3);
			break;
		}
	
	}
}  
 
  
  
  
