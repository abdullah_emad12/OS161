/*
 * Copyright (c) 2013
 *	The President and Fellows of Harvard College.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef _PROC_H_
#define _PROC_H_

/*
 * Definition of a process.
 *
 * Note: curproc is defined by <current.h>.
 */
 
#define PID_LIMIT 4096
#define PROC_LIMIT 1024
#define PROC_CHILD_LIMIT 512
#define PID_CHUNCKS_THRESH 50
#define PROC_MAX_OPENED_FILES 1023

#include <spinlock.h>
#include <vnode.h>
#include <hashtable.h>
#include <cranbtree.h>
#include <synch.h>

struct addrspace;
struct thread;
struct vnode;

/*
 * Process structure.
 *
 * Note that we only count the number of threads in each process.
 * (And, unless you implement multithreaded user processes, this
 * number will not exceed 1 except in kproc.) If you want to know
 * exactly which threads are in the process, e.g. for debugging, add
 * an array and a sleeplock to protect it. (You can't use a spinlock
 * to protect an array because arrays need to be able to call
 * kmalloc.)
 *
 * You will most likely be adding stuff to this structure, so you may
 * find you need a sleeplock in here for other reasons as well.
 * However, note that p_addrspace must be protected by a spinlock:
 * thread_switch needs to be able to fetch the current address space
 * without sleeping.
 */
struct proc {
	char *p_name;			/* Name of this process */
	struct spinlock p_lock;		/* Lock for this structure */
	unsigned p_numthreads;		/* Number of threads in this process */

	/* VM */
	struct addrspace *p_addrspace;	/* virtual address space */

	/* VFS */
	struct vnode *p_cwd;		/* current working directory */

	uint16_t p_id ;		/*Process_id*/

	struct cranbtree* p_cbt_fds; /*All the file descriptiors associated with the process*/
	
	struct linked_list* p_free_fds; /*list of all free that can be used to open new files*/

	struct proc* p_parent; /*refers to the parent of this process, if any*/
	struct linked_list* p_ll_children; /*a list of the children of this process if any*/

	bool p_terminated; /*indicates that the process has terminated (useful for the parent)*/
	int p_exit_status; /*the exit code status*/
	struct cv* p_cv_waitpid; /*used in waitpid by the parent*/
	struct lock* p_cv_lock;
};



/*
 * This represents a chunk of valid pids
 * Containts an upper and lower limits on the valid pid 
 * 
 * Example: a chunck with lower limit = 5 and upper limit = 20 means that pids 5, 6, 7, 8, ...., 20
 * are available for allocation
 */
 
struct pid_chunck
{
	uint16_t ck_lower;
	uint16_t ck_upper;
};

/*
 * Process Table 
 * This datastructure stores all the processes, and keeps track of all the pid number
 *
 */
 struct process_table
 {
 	struct cranbtree* p_cbt; /*hash table containing all the processes*/
	struct linked_list* pid_ll; /*List of all the available pid chuncks. Initially one chunck from 2-32768*/ 
 };


/*have the process table visible everywhere in the kernel*/
extern struct process_table p_table;


/* This is the process structure for the kernel and for kernel-only threads. */
extern struct proc* kproc;

/* Call once during system startup to allocate data structures. */
void proc_bootstrap(void);

/*Call once when the system is shutting down*/
void proc_cleanup(void);

/* Create a fresh process for use by runprogram(). */
struct proc *proc_create_runprogram(const char *name);

/* Destroy a process. */
void proc_destroy(struct proc *proc);

/* Attach a thread to a process. Must not already have a process. */
int proc_addthread(struct proc *proc, struct thread *t);

/* Detach a thread from its process. */
void proc_remthread(struct thread *t);

/* Fetch the address space of the current process. */
struct addrspace *proc_getas(void);

/* Change the address space of the current process, and return the old one. */
struct addrspace *proc_setas(struct addrspace *);


#endif /* _PROC_H_ */
