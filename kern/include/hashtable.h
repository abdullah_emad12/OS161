/*
 * NO NOT MODIFY THIS FILE
 *
 * All the contents of this file are overwritten during automated
 * testing. Please consider this before changing anything in this file.
 */

/*
 * Copyright (c) 2000, 2001, 2002, 2003, 2004, 2005, 2008, 2009
 *	The President and Fellows of Harvard College.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
 
#ifndef _HASHTABLE_H
#define _HASHTABLE_H

#define DEFAULT_HT_SIZE 20
#include <linkedlist.h>


/**
  * Struct for the hashtable
  */
typedef struct hashtable 
{
	int ht_size; /*number of buckets*/
	int ht_length; /*number of objects*/
	struct linked_list* ht_buckets[];
}hashtable_t;


/**
  * int -> struct hashtable*
  *
  * Creates and initializes a hash table with 'size' buckets 
  * returns a pointer to the hashtable
  *
  * Note: size must be greater than 0;
  */
struct hashtable* hashtable_create(const int size);



/**
  * struct hashtable* -> void
  * 
  * Deletes all the memory associated with the hashtable 
  * 
  * Note: does not free objects located in the hashtable's buckets
  */

void hashtable_destroy(struct hashtable* ht, void (* destroy_object)(void*));


/**
  * struct hashtable*, void* , const char* -> void
  * inserts a given object with a given key into the hashtable
  * 
  * struct hashtable* ht: the hashtable at whicht the object is going to be inserted
  * void* object: the object that is going to be inserted
  * const char* key: the key that is going to be hashed
  */

void hashtable_insert(struct hashtable* ht, void* object, const char* key);


/**
  * struct hashtable*, void* , const char* -> void
  * removes a given object with a given key into the hashtable
  * 
  * returns the object that was removed
  * 
  * struct hashtable* ht: the hashtable at whicht the object is going to be inserted
  * void* object: the object that is going to be removed
  * const char* key: the key that is going to be hashed
  */
void* hashtable_remove(struct hashtable* ht, void* object, const char* key);


/**
  * struct hashtable*, void* , const char* -> void
  * returns true if the given object was found in the hashtable, false otherwise
  * 
  * struct hashtable* ht: the hashtable at whicht the object of interest
  * void* object: the object that is going to be found (or not)
  * const char* key: the hash search ket
  *
  */
bool contains(struct hashtable* ht, void* object, const char* key);

/**
  * struct hashtable* -> unsinged int
  * returns the number of elements in the hashtable
  *
  */
unsigned int hashtable_length(struct hashtable* ht);

#endif /*_HASHTABLE_H*/
