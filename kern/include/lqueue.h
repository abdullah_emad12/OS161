/*
 * Author: Abdullah Emad
 *
 * Copyright (c) 2000, 2001, 2002, 2003, 2004, 2005, 2008, 2009
 *	The President and Fellows of Harvard College.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
 
#ifndef _LQUEUE_H_
#define _LQUEUE_H_

/*the FIFO queue is defined as a linked list*/
typedef struct lqueue
{
	struct lnode* head;
	struct lnode* tail; 
	int length;
}lqueue_t;

/*A node in the linked list*/
struct lnode
{
	void* object;
	struct lnode* next;
};



/* lqueue_create()
 * void -> lqueue*
 * returns a pointer to an empty queue
 *
 */
 
struct lqueue* lqueue_create(void);


/* lqueue_destroy(lqueue)
 * lqueue* -> void
 * Frees all the memory associated with lqeueue
 *
 * Note: this function will not free any memory associated with the object pointed to by the node
 */
 void lqueue_destroy(struct lqueue* lqueue);
 
 
 
 /* lqueue_enqueue()
  * struct lqueue*, void* -> void
  * Appends new object to the tail of the list
  *
  */
void lqueue_enqueue(struct lqueue* lqueue, void* object);



/* lqueue_dequeue()
 * struct lqueue* -> void*
 * returns the first object in the list after removing it
 */
void* lqueue_dequeue(struct lqueue* lqueue);

#endif
