/*
 * Copyright (c) 2000, 2001, 2002, 2003, 2004, 2005, 2008, 2009
 *	The President and Fellows of Harvard College.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef _SYSCALL_H_
#define _SYSCALL_H_


#include <cdefs.h> /* for __DEAD */
#include <types.h>
struct trapframe; /* from <machine/trapframe.h> */

/*
 * The system call dispatcher.
 */

void syscall(struct trapframe *tf);

/*
 * Support functions.
 */

/* Helper for fork(). You write this. */
void fork_entrypoint(void* obj, long unsigned junk);
void enter_forked_process(struct trapframe *tf);

/* Enter user mode. Does not return. */
__DEAD void enter_new_process(int argc, userptr_t argv, userptr_t env,
		       vaddr_t stackptr, vaddr_t entrypoint);


/*
 * Prototypes for IN-KERNEL entry points for system call implementations.
 */

int sys_reboot(int code);
int sys___time(userptr_t user_seconds, userptr_t user_nanoseconds);


/*
 * Prototypes for File system support system call implementations
 */


 
 /**
   * const char*, int, int* -> int
   * EFFECTS: open opens the file, device, or other kernel object named by the pathname filename. 
   * The flags argument specifies how to open the file.
   * RETURN: returns zero on success, and -1 on failure
   * REQIURES: flags to be set to eith read, write or readwrite
   * MODIFIES: fd
   * PARAMATERS:
   * 	- const char* filename: the name of the file to be opened
   *	- int flags: flags that specifies how to open the file (read man)
   *	- pointer to the fd that will be set
   */
int sys_open(const char *filename, int flags, int* fd);

 
 /**
   * int, void*, size_t, int* -> ssize_t
   * EFFECTS: Reads at most `buflen` bytes from the file specified by the fd into buf
   * MODIFIES: buf
   * REQUIRES: buf to point to a valid buffer and buflen be at most the size of the buffer
   * RETURNS: 0 on success and error code according to the man on failure. 
   *		  Additionally ret will be set to total number of bytes read
   * PARAMETERS: 
   * - int fd: file descriptor
   * - int buf: the buffer that will hold the read data
   * - size_t buflen: length of the data to read
   * - int* ret: the size of the data read in bytes
   */
int sys_read(int fd, void *buf, size_t buflen, ssize_t* ret);
 
 
 /**
   * int, void*, size_t, int* -> ssize_t
   * EFFECTS: writes at most `buflen` bytes from the file specified by the fd from buf
   * MODIFIES: file pointed to by fd, ret
   * REQUIRES: buf to point to a valid buffer and buflen be at most the size of the buffer
   * RETURNS: 0 on success and error code according to the man on failure. 
   *		  Additionally ret will be set to total number of bytes written
   * PARAMETERS: 
   * - int fd: file descriptor
   * - int buf: the buffer that will hold the write data
   * - size_t buflen: length of the data to write
   * - int* ret: the size of the data written in bytes
   */
int sys_write(int fd, const void *buf, size_t buflen, ssize_t* ret);
 
 /**
   * int, int, int, int -> off_t
   * EFFECTS: change the offset according to the given whence and seek value
   * REQUIRES: The object to be seeked to be a file not a device
   * MODIFIES: filehandle associated with fd
   * RETURNS: 0 on success, or an error code according to the man. ret will be set to amount of bytes seeked
   * PARAMETERS: 
   * - int fd: file descriptor associated with the file handle
   * - off_t pos: the new position in the file (in bytes)
   * - int whence: where to start seeking
   * - off_t* ret: the amount that was actaully seeked
   */
int sys_lseek(int fd, off_t pos, int whence, int32_t* ret, int32_t* ret2);
 
 
 /**
   * int , int* -> int
   * EFFECTS: Removes the filehandle and closes the file associated with the file descriptor
   * REQUIRES: a valid file descriptor
   * MODIFIES: proc->p_cbt_fds
   * RETURN:  On success, close returns 0. On error, the corresponding error code is returned (look at the manual).
   * PARAMETERS: 
   * - int fd: file descriptor
   * - int* ret: return value of close. 0 on success, -1 on failure
   */
int sys_close(int fd, int* ret);
 
 /**
   * int, int, int* -> int
   *  EFFECTS: dup2 clones the file handle identifed by file descriptor oldfd onto the file handle identified by newfd.
   * 		   If newfd names an already-open file, that file is closed.
   * MODIFIES:  proc->p_cbt_fds
   * RETURNS: 0 on success, or an error code on failure
   */
int sys_dup2(int oldfd, int newfd, int* ret);

 
 
 /**
   * const char* -> int
   * EFFECTS: changes the current working directory of a certain process to the new one
   * MODIFIES: curproc
   * RETURNS: 0 on success, the error code otherwise
   *
   */
int sys_chdir(const char *pathname);
 
 
 
/**
  * char*, size_t, int* -> int
  * EFFECTS: stores the path of the current directory in char* buf
  * MODIFIES: char* buf, int* ret
  * RETURNS: 0 on success, the error code according to man pages on failure
  * PARAMETES: 
  * - char* buf: buffer where the path name will be stored 
  * - size_t buflen: the length of the buffer
  * - int* ret: used to return the number of the bytes written to char* buf
  *
  */
int sys___getcwd(char *buf, size_t buflen, int* ret);


/**
  * char*, mode_t -> int
  * EFFECTS: creates a new directory with the given path
  * RETURNS: 0 on success, error code on failure
  */
int sys_mkdir(const char *pathname, mode_t mode);


/**
  * char* -> int
  * EFFECTS: removes an existing directory with the given path
  * RETURNS: 0 on success, error code on failure
  */
int sys_rmdir(const char *pathname);

/**
  * const char*, int* -> int
  * EFFECTS: removes a file with the given name
  * RETRUNS: zero on success, or error code on failure
  * PARAREMETERS:
  *  - const char* path: the path to the file 
  *  - int* ret: pointer to the return value
  */
int sys_remove(const char* path, int* ret);

/************************************************************
* Prototypes for process support system call implementation *
*************************************************************/

/**
  * void -> pid_t
  * RETURNS: the process id of the current process
  *
  */
pid_t sys_getpid(void);


/**
  * struct trapframe*, pid_t* -> int
  * EFFECTS: creates an exact clone of the calling process and starts running it
  *			 For a complete description of the specifications of the fork process
  *			 read the manual.
  * MODIFIES: stores the child pid in pid_t* ret to be returned to the parent
  * RETURNS: 0 on sucess, error code on failure (look at the man for the error codes)
  */
int sys_fork(struct trapframe* tf, pid_t* pid);

/** 
  * int -> void
  * EFFECTS: Terminates a process. If it has a parent and its parent is still running, it wil 
  *		 	 just mark itself as terminated and set its exit status. Otherwise it will destroy itself.
  *		 	 In all cases, it will also scan its children and destroy the ones marked terminated.
  * MODIFIES: curproc
  */ 
void sys__exit(int exitcode, int exitState);


/**
  * pid_t, int*, int, pid_t* -> int
  * EFFECTS: Wait for the process specified by pid to exit, 
  *		 	 and return an encoded exit status in the integer pointed to by status.
  * REQUIRES: read the man
  * MODIFIES: stores the pid of the target process in pid_t* ret
  * RETURNS: 0 on sucess, error code on according to the manual on failure.
  *
  */
int sys_waitpid(pid_t pid, userptr_t status, int options, pid_t* ret);

/**
  * const char* , char** -> progname
  * EFFECTS: replace the current prorgram's address space with a new one containing a
  * 		 a program specified by the programname
  * REQUIRES: programname to be refering to a program
  * MODIFIES: curproc
  * RETURNS: error code on failure. It should not return on sucess
  */
int sys_execv(const char* progname, char** argv);

#endif /* _SYSCALL_H_ */
