/*
 * Copyright (c) 2000, 2001, 2002, 2003, 2004, 2005, 2008, 2009
 *	The President and Fellows of Harvard College.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/*
 * Synchronization primitives.
 * The specifications of the functions are in synch.h.
 */

#include <types.h>
#include <lib.h>
#include <spinlock.h>
#include <wchan.h>
#include <thread.h>
#include <current.h>
#include <synch.h>


////////////////////////////////////////////////////////////
//
// Semaphore.

struct semaphore* sem_create(const char *name, unsigned initial_count)
{
	struct semaphore *sem;

	sem = kmalloc(sizeof(*sem));
	if (sem == NULL) {
		return NULL;
	}

	sem->sem_name = kstrdup(name);
	if (sem->sem_name == NULL) {
		kfree(sem);
		return NULL;
	}

	sem->sem_wchan = wchan_create(sem->sem_name);
	if (sem->sem_wchan == NULL) {
		kfree(sem->sem_name);
		kfree(sem);
		return NULL;
	}

	spinlock_init(&sem->sem_lock);
	sem->sem_count = initial_count;

	return sem;
}

void sem_destroy(struct semaphore *sem)
{
	KASSERT(sem != NULL);

	/* wchan_cleanup will assert if anyone's waiting on it */
	spinlock_cleanup(&sem->sem_lock);
	wchan_destroy(sem->sem_wchan);
	kfree(sem->sem_name);
	kfree(sem);
}

void P(struct semaphore *sem)
{
	KASSERT(sem != NULL);

	/*
	 * May not block in an interrupt handler.
	 *
	 * For robustness, always check, even if we can actually
	 * complete the P without blocking.
	 */
	KASSERT(curthread->t_in_interrupt == false);

	/* Use the semaphore spinlock to protect the wchan as well. */
	spinlock_acquire(&sem->sem_lock);
	while (sem->sem_count == 0) {
		/*
		 *
		 * Note that we don't maintain strict FIFO ordering of
		 * threads going through the semaphore; that is, we
		 * might "get" it on the first try even if other
		 * threads are waiting. Apparently according to some
		 * textbooks semaphores must for some reason have
		 * strict ordering. Too bad. :-)
		 *
		 * Exercise: how would you implement strict FIFO
		 * ordering?
		 */
		wchan_sleep(sem->sem_wchan, &sem->sem_lock);
	}
	KASSERT(sem->sem_count > 0);
	sem->sem_count--;
	spinlock_release(&sem->sem_lock);
}

void V(struct semaphore *sem)
{
	KASSERT(sem != NULL);

	spinlock_acquire(&sem->sem_lock);

	sem->sem_count++;
	KASSERT(sem->sem_count > 0);
	wchan_wakeone(sem->sem_wchan, &sem->sem_lock);

	spinlock_release(&sem->sem_lock);
}

////////////////////////////////////////////////////////////
//
// Lock.

struct lock* lock_create(const char *name)
{
	struct lock *lock;

	lock = kmalloc(sizeof(*lock));
	if (lock == NULL) {
		return NULL;
	}

	lock->lk_name = kstrdup(name);
	if (lock->lk_name == NULL) {
		kfree(lock);
		return NULL;
	}
	
	lock->lk_wchan = wchan_create(name);
	if (lock->lk_wchan == NULL) {
		kfree(lock->lk_name);
		kfree(lock);
		return NULL;
	}

	lock->lk_acq_thread = NULL;
	HANGMAN_LOCKABLEINIT(&lock->lk_hangman, lock->lk_name);

	spinlock_init(&lock->lk_lock);
	lock->lk_queue = lqueue_create();
	

	return lock;
}

void lock_destroy(struct lock *lock)
{
	KASSERT(lock != NULL);
	KASSERT(lock->lk_acq_thread == NULL);
	KASSERT(lock->lk_queue->length == 0);
	
	spinlock_cleanup(&lock->lk_lock);
	lqueue_destroy(lock->lk_queue);


	wchan_destroy(lock->lk_wchan);
	kfree(lock->lk_name);
	kfree(lock);
}

void lock_acquire(struct lock *lock)
{
	KASSERT(lock != NULL);
	KASSERT(curthread != NULL);
	
	spinlock_acquire(&lock->lk_lock);
	struct thread* thread = curthread;
	
	/*It is the only thread (or the first)*/
	if(lock->lk_acq_thread == NULL)
	{
		lock->lk_acq_thread = thread;
		spinlock_release(&lock->lk_lock);
		return;
	}
	
	
	/*pushes this thread on the queue*/
	lqueue_enqueue(lock->lk_queue, thread);
	/* Call this (atomically) before waiting for a lock */
	HANGMAN_WAIT(&curthread->t_hangman, &lock->lk_hangman);

	/*sleeps until some other thread wakes it up*/
	while(lock->lk_acq_thread != thread)	
	{
		wchan_sleep(lock->lk_wchan, &lock->lk_lock);
	}	

	/* Call this (atomically) once the lock is acquired */
	HANGMAN_ACQUIRE(&curthread->t_hangman, &lock->lk_hangman);
	spinlock_release(&lock->lk_lock);
	

}

void lock_release(struct lock *lock)
{
	// Lock can only be released by the acquirer
	KASSERT(lock->lk_acq_thread == curthread);

	spinlock_acquire(&lock->lk_lock);
	lock->lk_acq_thread = lqueue_dequeue(lock->lk_queue);
	wchan_wakeall(lock->lk_wchan,&lock->lk_lock);
	/* Call this (atomically) when the lock is released */
	HANGMAN_RELEASE(&curthread->t_hangman, &lock->lk_hangman);
	spinlock_release(&lock->lk_lock);
}

bool lock_do_i_hold(struct lock *lock)
{
	(void)lock;  // suppress warning until code gets written
	
	return lock->lk_acq_thread == curthread;
}

////////////////////////////////////////////////////////////
//
// CV


struct cv * cv_create(const char *name)
{
	struct cv *cv;

	cv = kmalloc(sizeof(*cv));
	if (cv == NULL) {
		return NULL;
	}

	cv->cv_name = kstrdup(name);
	if (cv->cv_name==NULL) {
		kfree(cv);
		return NULL;
	}

	cv->cv_wchan = wchan_create(name);
	if (cv->cv_wchan == NULL) {
		kfree(cv->cv_name);
		kfree(cv);
		return NULL;
	}
	
	spinlock_init(&cv->cv_lock);

	return cv;
}

void cv_destroy(struct cv *cv)
{
	KASSERT(cv != NULL);

	spinlock_cleanup(&cv->cv_lock);
	wchan_destroy(cv->cv_wchan);
	
	kfree(cv->cv_name);
	kfree(cv);
}

void cv_wait(struct cv *cv, struct lock *lock)
{
	KASSERT(cv != NULL);
	KASSERT(lock != NULL);
	KASSERT(lock_do_i_hold(lock));
	
	spinlock_acquire(&cv->cv_lock);
	
	// releases the lock
	lock_release(lock);

	
	// Go to sleep until the other thread wakes it up
	wchan_sleep(cv->cv_wchan, &cv->cv_lock);

	spinlock_release(&cv->cv_lock);
	lock_acquire(lock);
}

void cv_signal(struct cv *cv, struct lock *lock)
{
	KASSERT(lock_do_i_hold(lock));
	spinlock_acquire(&cv->cv_lock);
	wchan_wakeone(cv->cv_wchan, &cv->cv_lock);
	spinlock_release(&cv->cv_lock);
}

void cv_broadcast(struct cv *cv, struct lock *lock)
{
	KASSERT(lock_do_i_hold(lock));
	spinlock_acquire(&cv->cv_lock);
	wchan_wakeall(cv->cv_wchan, &cv->cv_lock);
	spinlock_release(&cv->cv_lock);
}

////////////////////////////////////////////////////////////
//
// RW


struct rwlock* rwlock_create(const char* name)
{
	KASSERT(name != NULL);
		
	// allocates memory for the lock
	struct rwlock* rw = kmalloc(sizeof(struct rwlock));
	if(rw == NULL)
	{
		return NULL;
	}
	
	// Creates the lock name
	rw->rwlock_name = kstrdup(name);
	if(rw->rwlock_name == NULL)
	{
		kfree(rw);
		return NULL;
	}
	
	rw->rw_current_mode = none;
	rw->rw_r_slots  = RW_R_THRESH;
	
	// creates waiting channels
	rw->rw_r_wchan = wchan_create(name);
	if(rw->rw_r_wchan == NULL)
	{
		kfree(rw->rwlock_name);
		kfree(rw);
		return NULL;
	}
	
	rw->rw_w_wchan = wchan_create(name);
	if(rw->rw_w_wchan == NULL)
	{
		wchan_destroy(rw->rw_r_wchan);
		kfree(rw->rwlock_name);
		kfree(rw);
		return NULL;	
	}
	rw->rw_acq_ht = hashtable_create(DEFAULT_HT_SIZE);
	if(rw->rw_acq_ht == NULL)
	{
		wchan_destroy(rw->rw_w_wchan);
		wchan_destroy(rw->rw_r_wchan);
		kfree(rw->rwlock_name);
		kfree(rw);
		return NULL;	
	}
	// initialize the spinlock
	spinlock_init(&rw->rw_lock);
	
	return rw;
}


void rwlock_destroy(struct rwlock* rw)
{
	KASSERT(rw != NULL);
	KASSERT(hashtable_length(rw->rw_acq_ht) == 0); /*No one should be holding this lock when releasing*/

	
	// destorys the struct's elements
	wchan_destroy(rw->rw_r_wchan);
	wchan_destroy(rw->rw_w_wchan);
	hashtable_destroy(rw->rw_acq_ht, NULL);
	spinlock_cleanup(&rw->rw_lock);
	kfree(rw->rwlock_name);
	kfree(rw);
}


void rwlock_acquire_read(struct rwlock* rw)
{
	KASSERT(rw != NULL);
	KASSERT(!contains(rw->rw_acq_ht, curthread, curthread->t_name));
	spinlock_acquire(&rw->rw_lock);
	
	/* number of waiting threads*/
	unsigned int n_readers = wchan_size(rw->rw_r_wchan, &rw->rw_lock);
	unsigned int n_writers = wchan_size(rw->rw_w_wchan, &rw->rw_lock);
	
	/*gives priority to the writer if number of writers > readers*/
	if(n_writers > n_readers + RW_R_THRESH)
	{
		// wakes up one writer if the mode is none
		if(rw->rw_current_mode == none)
		{
			wchan_wakeone(rw->rw_w_wchan, &rw->rw_lock);
		}
		rw->rw_r_slots = 0;
	}
	
	/*keeps sleeping while there are no available slots and the writers wchan is not empty*/
	while((rw->rw_r_slots <= 0 && !wchan_isempty(rw->rw_w_wchan, &rw->rw_lock)) || rw->rw_current_mode == write)
	{
			wchan_sleep(rw->rw_r_wchan, &rw->rw_lock);
	}
	

	// acquires the lock
	rw->rw_current_mode = read;
	hashtable_insert(rw->rw_acq_ht, curthread, curthread->t_name);
	rw->rw_r_slots = rw->rw_r_slots <= 0 ? 0 : rw->rw_r_slots - 1; // prevents overflow
	
	spinlock_release(&rw->rw_lock);
}
void rwlock_release_read(struct rwlock* rw)
{
	KASSERT(rw != NULL);
	spinlock_acquire(&rw->rw_lock);
	KASSERT(contains(rw->rw_acq_ht, curthread, curthread->t_name));
	/*release the lock*/
	hashtable_remove(rw->rw_acq_ht, curthread, curthread->t_name);
	
	if(hashtable_length(rw->rw_acq_ht) == 0)
	{
		rw->rw_current_mode = none;
	}
	/*decides whether to wake a reader or a writer*/
	if(!wchan_isempty(rw->rw_r_wchan, &rw->rw_lock) && rw->rw_r_slots > 0 && rw->rw_current_mode != write)
	{
		wchan_wakeall(rw->rw_r_wchan, &rw->rw_lock); //wakes all readers
	}
	else if(rw->rw_current_mode == none)
	{
		wchan_wakeone(rw->rw_w_wchan, &rw->rw_lock); // wakes one writer
	}
	spinlock_release(&rw->rw_lock);
}
void rwlock_acquire_write(struct rwlock* rw)
{
	KASSERT(rw != NULL);
	spinlock_acquire(&rw->rw_lock);
	KASSERT(!contains(rw->rw_acq_ht, curthread, curthread->t_name));
	// keeps sleeping if other thread was acquiring the lock
	while(rw->rw_current_mode != none)
	{
		wchan_sleep(rw->rw_w_wchan, &rw->rw_lock);
	}
	rw->rw_current_mode = write;
	hashtable_insert(rw->rw_acq_ht, curthread, curthread->t_name);
	KASSERT(hashtable_length(rw->rw_acq_ht) == 1); /*only one thread must be acquiring this*/
	rw->rw_r_slots = RW_R_THRESH; /*refills the number of readers slots*/
	
	spinlock_release(&rw->rw_lock);
}
void rwlock_release_write(struct rwlock* rw)
{
	KASSERT(rw != NULL);
	KASSERT(contains(rw->rw_acq_ht, curthread, curthread->t_name));
	spinlock_acquire(&rw->rw_lock);
	
	unsigned int n_readers = wchan_size(rw->rw_r_wchan, &rw->rw_lock);
	unsigned int n_writers = wchan_size(rw->rw_w_wchan, &rw->rw_lock);
	
	/*release the lock*/
	hashtable_remove(rw->rw_acq_ht, curthread, curthread->t_name);
	KASSERT(hashtable_length(rw->rw_acq_ht) == 0);
	rw->rw_current_mode = none;
	
	/*decides whether to wake a reader or a writer*/
	if(n_writers > n_readers + RW_R_THRESH || wchan_isempty(rw->rw_r_wchan, &rw->rw_lock))
	{
		wchan_wakeone(rw->rw_w_wchan, &rw->rw_lock); // wakes one writer
	}
	else
	{
		wchan_wakeall(rw->rw_r_wchan, &rw->rw_lock); //wakes all readers
	}
	spinlock_release(&rw->rw_lock);
}
