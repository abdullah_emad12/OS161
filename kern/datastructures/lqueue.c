/*
 * Author: Abudullah Emad
 * 
 * Copyright (c) 2000, 2001, 2002, 2003, 2004, 2005, 2008, 2009, 2010
 *	The President and Fellows of Harvard College.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */


/*
 * Note: this queue was created to maintain FIFO order
 */

#include <lqueue.h>
#include <types.h>
#include <lib.h>

/*
 * returns a pointer to an empty queue
 */

struct lqueue* lqueue_create(void)
{
	struct lqueue* lqueue = kmalloc(sizeof(struct lqueue));
	if(lqueue == NULL)
	{
		return NULL;
	}

	lqueue->head = NULL;
	lqueue->tail = NULL;
	lqueue->length = 0;
	return lqueue;
}

static void lqueue_destroy_nodes(struct lnode* node)
{
	if(node == NULL)
	{
		return;
	}
	
	lqueue_destroy_nodes(node->next);
	kfree(node);
}

/*
 * Frees all the memory associated with lqeueue
 */
void lqueue_destroy(struct lqueue* lqueue)
{
	KASSERT(lqueue != NULL);
	lqueue_destroy_nodes(lqueue->head);
	kfree(lqueue);
}


/* 
 * Appends new object to the tail of the list
 */
void lqueue_enqueue(struct lqueue* lqueue, void* object)
{
	// some Assertions
	KASSERT(lqueue != NULL);
	KASSERT(object != NULL);

	// creates new node
	struct lnode* node = kmalloc(sizeof(struct lnode));
	if(node == NULL)
	{
		return;
	}
	// updates the tail of the queue
	node->object = object;
	node->next = NULL;
	if(lqueue->head == NULL)
	{
		lqueue->head = node;
	}
	else
	{
		lqueue->tail->next = node;
	}
	lqueue->tail = node;
	lqueue->length++;
}



/*
 * returns the first object in the list after removing it
 */
void* lqueue_dequeue(struct lqueue* lqueue)
{
	KASSERT(lqueue);

	if(lqueue->head == NULL)
	{
		return NULL;
	}
	// removes the first item from the list
	void* object = lqueue->head->object;
	struct lnode* first_node = lqueue->head;
	lqueue->head = lqueue->head->next;
	lqueue->length--;
	kfree(first_node);
	return object;
}






