#include <lqueue.h>
#include <types.h>
#include <lib.h>
#include <hashtable.h>

static unsigned int hash(void *string)
{
	/* This is the djb2 string hash function */

	unsigned int result = 5381;
	unsigned char *p;

	p = (unsigned char *) string;

	while (*p != '\0') {
		result = (result << 5) + result + *p;
		++p;
	}

	return result;
}

struct hashtable* hashtable_create(const int size)
{
	KASSERT(size > 0);

	struct hashtable* ht = kmalloc(sizeof(struct hashtable) + (size * sizeof(struct linked_list)));
	ht->ht_size = size;
	ht->ht_length = 0;

	for(int i = 0; i < size; i++)
	{
		ht->ht_buckets[i] = ll_create();
	}
	return ht;
}


void hashtable_destroy(struct hashtable* ht, void (* destroy_object)(void*))
{
	KASSERT(ht != NULL);

	for(int i = 0; i < ht->ht_size; i++)
	{
		ll_destroy(ht->ht_buckets[i], destroy_object);
	}

	kfree(ht);
}

void hashtable_insert(struct hashtable* ht, void* object, const char* key)
{
	KASSERT(ht != NULL);
	KASSERT(key != NULL);
	unsigned int index = hash((void*)key) % ht->ht_size;
	ht->ht_length++;
	ll_add(ht->ht_buckets[index], object);
}


void* hashtable_remove(struct hashtable* ht, void* object, const char* key)
{
	KASSERT(ht != NULL);
	KASSERT(key != NULL);
	unsigned int index = hash((void*)key) % ht->ht_size;
	ht->ht_length--;
	return ll_remove(ht->ht_buckets[index], ll_find(ht->ht_buckets[index], object));
	
}

bool contains(struct hashtable* ht, void* object, const char* key)
{
	KASSERT(ht != NULL);
	KASSERT(key != NULL);
	
	unsigned int index = hash((void*)key) % ht->ht_size;
	return ll_find(ht->ht_buckets[index], object) != NULL;

}

unsigned int hashtable_length(struct hashtable* ht)
{
	KASSERT(ht != NULL);
	return ht->ht_length;
}
