/*
 * Copyright (c) 2013
 *	The President and Fellows of Harvard College.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/*
 * Process support.
 *
 * There is (intentionally) not much here; you will need to add stuff
 * and maybe change around what's already present.
 *
 * p_lock is intended to be held when manipulating the pointers in the
 * proc structure, not while doing any significant work with the
 * things they point to. Rearrange this (and/or change it to be a
 * regular lock) as needed.
 *
 * Unless you're implementing multithreaded user processes, the only
 * process that will have more than one thread is the kernel process.
 */

#include <types.h>
#include <spl.h>
#include <proc.h>
#include <current.h>
#include <addrspace.h>
#include <vnode.h>
#include <kern/fcntl.h>
#include <kern/stat.h>
#include <vfs.h>
#include <filehandle.h>

/*Static functions prototypes*/
void pid_chuncks_restructure(void);
static void pid_free(uint16_t pid);
static uint16_t pid_alloc(void);
static void proc_manager_init(void);
static void open_std_channels(void);
static void close_std_channels(void);

/*
 * The process for the kernel; this holds all the kernel-only threads.
 */
struct proc* kproc;
struct process_table p_table;

/*STD channels filehandles*/
struct filehandle* stdin_fh = NULL;
struct filehandle* stdout_fh = NULL;
struct filehandle* stderr_fh = NULL;

/*
 * Cleans up the global variable associated with the process manager
 * 
 * Must get called when the kernel shutsdown or reboot
 */
void proc_cleanup(void)
{
	cbt_destroy(p_table.p_cbt, (void (*)(void*))proc_destroy);
	ll_destroy(p_table.pid_ll, kfree);
	close_std_channels();
	kprintf("Process Table Destroyed...\n");
	
}


/*
 * Create a proc structure.
 */
static struct proc* proc_create(const char *name)
{
	struct proc *proc;

	proc = kmalloc(sizeof(*proc));
	if (proc == NULL) {
		return NULL;
	}
	proc->p_name = kstrdup(name);
	if (proc->p_name == NULL) {
		kfree(proc);
		return NULL;
	}

	proc->p_numthreads = 0;
	spinlock_init(&proc->p_lock);

	/* VM fields */
	proc->p_addrspace = NULL;

	/* VFS fields */
	proc->p_cwd = NULL;


	proc->p_id = pid_alloc();
	
	/*create list of free fds*/
	proc->p_free_fds = ll_create();
	if(proc->p_free_fds == NULL)
	{
		spinlock_cleanup(&proc->p_lock);
		kfree(proc->p_name);
		kfree(proc);
		return NULL;
	}
	
	/*create the fd tree*/
	proc->p_cbt_fds = cbt_create(5);
	if(proc->p_cbt_fds == NULL)
	{
		spinlock_cleanup(&proc->p_lock);
		ll_destroy(proc->p_free_fds, NULL);
		kfree(proc->p_name);
		kfree(proc);
		return NULL;
	}
	
	proc->p_parent = NULL;
	proc->p_ll_children = ll_create();
	if(proc->p_ll_children == NULL)
	{
		spinlock_cleanup(&proc->p_lock);
		ll_destroy(proc->p_free_fds, NULL);
		kfree(proc->p_name);
		kfree(proc);
		cbt_destroy(proc->p_cbt_fds, NULL);
		return NULL;
	}
	
	proc->p_terminated = false;
	proc->p_exit_status = 0;
	
	proc->p_cv_waitpid = cv_create(proc->p_name);
	if(proc->p_cv_waitpid == NULL)
	{
		ll_destroy(proc->p_ll_children, NULL);
		spinlock_cleanup(&proc->p_lock);
		ll_destroy(proc->p_free_fds, NULL);
		kfree(proc->p_name);
		kfree(proc);
		cbt_destroy(proc->p_cbt_fds, NULL);
		return NULL;
	}	
	
	proc->p_cv_lock = lock_create(proc->p_name);
	if(proc->p_cv_lock == NULL)
	{
		cv_destroy(proc->p_cv_waitpid);
		ll_destroy(proc->p_ll_children, NULL);
		spinlock_cleanup(&proc->p_lock);
		ll_destroy(proc->p_free_fds, NULL);
		kfree(proc->p_name);
		kfree(proc);
		cbt_destroy(proc->p_cbt_fds, NULL);
		return NULL;
	}

	return proc;
}

/*
 * Destroy a proc structure.
 *
 * Note: nothing currently calls this. Your wait/exit code will
 * probably want to do so.
 */
void proc_destroy(struct proc *proc)
{
	/*
	 * You probably want to destroy and null out much of the
	 * process (particularly the address space) at exit time if
	 * your wait/exit design calls for the process structure to
	 * hang around beyond process exit. Some wait/exit designs
	 * do, some don't.
	 */

	KASSERT(proc != NULL);
	KASSERT(proc != kproc);

	/*
	 * We don't take p_lock in here because we must have the only
	 * reference to this structure. (Otherwise it would be
	 * incorrect to destroy it.)
	 */

	/* VFS fields */
	if (proc->p_cwd) {
		VOP_DECREF(proc->p_cwd);
		proc->p_cwd = NULL;
	}
	pid_free(proc->p_id);

	/* VM fields */
	if (proc->p_addrspace) {
		/*
		 * If p is the current process, remove it safely from
		 * p_addrspace before destroying it. This makes sure
		 * we don't try to activate the address space while
		 * it's being destroyed.
		 *
		 * Also explicitly deactivate, because setting the
		 * address space to NULL won't necessarily do that.
		 *
		 * (When the address space is NULL, it means the
		 * process is kernel-only; in that case it is normally
		 * ok if the MMU and MMU- related data structures
		 * still refer to the address space of the last
		 * process that had one. Then you save work if that
		 * process is the next one to run, which isn't
		 * uncommon. However, here we're going to destroy the
		 * address space, so we need to make sure that nothing
		 * in the VM system still refers to it.)
		 *
		 * The call to as_deactivate() must come after we
		 * clear the address space, or a timer interrupt might
		 * reactivate the old address space again behind our
		 * back.
		 *
		 * If p is not the current process, still remove it
		 * from p_addrspace before destroying it as a
		 * precaution. Note that if p is not the current
		 * process, in order to be here p must either have
		 * never run (e.g. cleaning up after fork failed) or
		 * have finished running and exited. It is quite
		 * incorrect to destroy the proc structure of some
		 * random other process while it's still running...
		 */
		struct addrspace *as;

		if (proc == curproc) {
			as = proc_setas(NULL);
			as_deactivate();
		}
		else {
			as = proc->p_addrspace;
			proc->p_addrspace = NULL;
		}
		as_destroy(as);
	}

	ll_destroy(proc->p_free_fds, kfree);
	cbt_destroy(proc->p_cbt_fds, filehandle_destroy);
	KASSERT(proc->p_numthreads == 0);
	spinlock_cleanup(&proc->p_lock);
	ll_destroy(proc->p_ll_children, NULL);

	void* object = cbt_delete(p_table.p_cbt, proc->p_id);
	KASSERT(object != NULL);

	cv_destroy(proc->p_cv_waitpid);
	lock_destroy(proc->p_cv_lock);
	kfree(proc->p_name);
	kfree(proc);
}

/*
 * Create the process structure for the kernel.
 */
void
proc_bootstrap(void)
{
	proc_manager_init();
	kproc = proc_create("[kernel]");
	if (kproc == NULL) {
		panic("proc_create for kproc failed\n");
	}
}

/*
 * Create a fresh proc for use by runprogram.
 *
 * It will have no address space and will inherit the current
 * process's (that is, the kernel menu's) current directory.
 */
struct proc *
proc_create_runprogram(const char *name)
{

	if(stdin_fh == NULL && stdout_fh == NULL && stderr_fh == NULL)
	{
		open_std_channels();
	}
	struct proc *newproc;

	newproc = proc_create(name);
	if (newproc == NULL) {
		return NULL;
	}

	/*inserts std filehandles*/
	cbt_insert(newproc->p_cbt_fds, 0, stdin_fh);
	cbt_insert(newproc->p_cbt_fds, 1, stdout_fh);
	cbt_insert(newproc->p_cbt_fds, 2, stderr_fh);

	/* VM fields */

	newproc->p_addrspace = NULL;

	/* VFS fields */

	/*
	 * Lock the current process to copy its current directory.
	 * (We don't need to lock the new process, though, as we have
	 * the only reference to it.)
	 */
	spinlock_acquire(&curproc->p_lock);
	if (curproc->p_cwd != NULL) {
		VOP_INCREF(curproc->p_cwd);
		newproc->p_cwd = curproc->p_cwd;
	}
	spinlock_release(&curproc->p_lock);

	/*insert in the process table*/
	cbt_insert(p_table.p_cbt, newproc->p_id , newproc);

	return newproc;
}

/*
 * Add a thread to a process. Either the thread or the process might
 * or might not be current.
 *
 * Turn off interrupts on the local cpu while changing t_proc, in
 * case it's current, to protect against the as_activate call in
 * the timer interrupt context switch, and any other implicit uses
 * of "curproc".
 */
int
proc_addthread(struct proc *proc, struct thread *t)
{
	int spl;

	KASSERT(t->t_proc == NULL);

	spinlock_acquire(&proc->p_lock);
	proc->p_numthreads++;
	spinlock_release(&proc->p_lock);

	spl = splhigh();
	t->t_proc = proc;
	splx(spl);

	return 0;
}

/*
 * Remove a thread from its process. Either the thread or the process
 * might or might not be current.
 *
 * Turn off interrupts on the local cpu while changing t_proc, in
 * case it's current, to protect against the as_activate call in
 * the timer interrupt context switch, and any other implicit uses
 * of "curproc".
 */
void
proc_remthread(struct thread *t)
{
	struct proc *proc;
	int spl;

	proc = t->t_proc;
	KASSERT(proc != NULL);

	spinlock_acquire(&proc->p_lock);
	KASSERT(proc->p_numthreads > 0);
	proc->p_numthreads--;
	spinlock_release(&proc->p_lock);

	spl = splhigh();
	t->t_proc = NULL;
	splx(spl);
}

/*
 * Fetch the address space of (the current) process.
 *
 * Caution: address spaces aren't refcounted. If you implement
 * multithreaded processes, make sure to set up a refcount scheme or
 * some other method to make this safe. Otherwise the returned address
 * space might disappear under you.
 */
struct addrspace *
proc_getas(void)
{
	struct addrspace *as;
	struct proc *proc = curproc;

	if (proc == NULL) {
		return NULL;
	}

	spinlock_acquire(&proc->p_lock);
	as = proc->p_addrspace;
	spinlock_release(&proc->p_lock);
	return as;
}

/*
 * Change the address space of (the current) process. Return the old
 * one for later restoration or disposal.
 */
struct addrspace *
proc_setas(struct addrspace *newas)
{
	struct addrspace *oldas;
	struct proc *proc = curproc;

	KASSERT(proc != NULL);

	spinlock_acquire(&proc->p_lock);
	oldas = proc->p_addrspace;
	proc->p_addrspace = newas;
	spinlock_release(&proc->p_lock);
	return oldas;
}

/**
  * opens the std channels to be used by all processes
  */ 
static void open_std_channels(void)
{
	/*stdin*/
	struct vnode* std_v;
	char* stdin_con = kstrdup("con:\0\0");
	int err = vfs_open(stdin_con, O_RDWR, S_IRUSR, &(std_v));
	KASSERT(err == 0);
	kfree(stdin_con);

	stdin_fh = filehandle_create_global(true, false);
	KASSERT(stdin_fh != NULL);
	stdin_fh->fh_vnode = std_v;

	stdout_fh = filehandle_create_global(false, true);
	KASSERT(stdout_fh != NULL);
	stdout_fh->fh_vnode = std_v;
	
	stderr_fh = filehandle_create_global(false, true);
	KASSERT(stdout_fh != NULL);
	stderr_fh->fh_vnode = std_v;

}


/**
  * closes the std channels 
  */

static void close_std_channels(void)
{
	if(stdin_fh != NULL)
	{
		stdin_fh->fh_global = false;
		filehandle_destroy(stdin_fh);
	}

	if(stdout_fh != NULL)
	{
		stdout_fh->fh_global = false;
		filehandle_destroy(stdout_fh);
	}

	if(stderr_fh != NULL)
	{
		stderr_fh->fh_global = false;
		filehandle_destroy(stderr_fh);
	}
}




/****************************************************
 *                                                  *
 *					PID Manager                     *
 *                                                  *
 ****************************************************/
 
 
 /*
 * initializes proc important variables and structures
 * 
 * Must get called when the kernel bootsup
 */

static void proc_manager_init(void)
{
	
	p_table.p_cbt = cbt_create(3);
	if(p_table.p_cbt == NULL)
	{
		panic("Couldn't initialize the process table\n");
	}
	
	p_table.pid_ll = ll_create();
	if(p_table.pid_ll == NULL)
	{
		panic("Couldn't initialize the process table\n");
	}
	struct pid_chunck* init_chunck = kmalloc(sizeof(struct pid_chunck)); /*initial PID chunck*/
	if(init_chunck == NULL)
	{
		panic("Couldn't initialize the process table\n");
	}
	init_chunck->ck_lower = 2;
	init_chunck->ck_upper = PID_LIMIT;
	ll_add(p_table.pid_ll, init_chunck);
	
	kprintf("Process Table initilized...\n");

}
 
 /**
   * void -> uint16_t 
   *
   * Search the pid list for a unique process id
   *
   * Returns a unique process ID. returns -1 if the process cannot be created
   * A reason would be that the maximum number of running processes was reached
   */
 static uint16_t pid_alloc(void)
 {
 	KASSERT(p_table.p_cbt != NULL);
 	if(cbt_get_length(p_table.p_cbt) == PROC_LIMIT)
 	{
 		return -1;
 	}
 	/*gets the first chunck of free pids*/
 	ll_node_t* chunck_node = ll_front(p_table.pid_ll);
	
	/*if somehow all the pids were consumed panic*/
	if(chunck_node == NULL)
	{
		panic("Woah! I am out of PIDs....\n");
	}

	struct pid_chunck* chunck = chunck_node->object;
	KASSERT(chunck->ck_lower <= chunck->ck_upper);
	 
	uint16_t pid = chunck->ck_lower;
	chunck->ck_lower++;
	 
	/*remove this chunck if all of its pids were consumed*/
	if(chunck->ck_lower > chunck->ck_upper)
	{
		ll_remove(p_table.pid_ll, chunck_node);
		kfree(chunck);
	}
	
	return pid;
 	
 }
 
 
 
 /*
  * uint16_t -> void
  * 
  * Frees PID to be reused by other new processes
  * 
  * Must be called when the process is terminating
  * 
  */
static void pid_free(uint16_t pid)
{

	ll_node_t* cursor = ll_front(p_table.pid_ll);
	
	/*Attempts to add the pid to existing chunck*/
	while(cursor != NULL)
	{

		
		
		struct pid_chunck* chunck = cursor->object;
		
		/*malformed chunck (panic)*/
		KASSERT(pid < chunck->ck_lower || pid > chunck->ck_upper);
		
		if(pid == chunck->ck_lower - 1)
		{
			chunck->ck_lower--;
			return;
		}
		else if(pid == chunck->ck_upper + 1)
		{
			chunck->ck_upper++;
			return;
		}
		cursor = cursor->next;
	}
	
	struct pid_chunck* new_chunck = kmalloc(sizeof(struct pid_chunck)); /*initial PID chunck*/
	if(new_chunck == NULL)
	{
		panic("Couldn't initialize a PID chunck\n");
	}
	new_chunck->ck_lower = pid;
	new_chunck->ck_upper = pid;
	ll_add(p_table.pid_ll, new_chunck); 

	/* restructures the chuncks list in an attempt to reduce the list size*/
	if(ll_length(p_table.pid_ll) >= PID_CHUNCKS_THRESH)
	{
		pid_chuncks_restructure();
	}

}
  
  
  
  
/**
  * void -> void 
  * Tries to combine multiple chuncks into one chunck
  *
  */
void pid_chuncks_restructure(void)
{

	ll_node_t* cursor = ll_front(p_table.pid_ll);
	
	/*Attempts to add the pid to existing chunck*/
	while(cursor != NULL)
	{
		struct pid_chunck* outer_chunck = cursor->object;
		ll_node_t* inner_cursor = cursor->next;
		
		/*Compares the chucnk against every other chunck and see if it can be combined*/
		while(inner_cursor != NULL)
		{
		
			struct pid_chunck* inner_chunck = cursor->object;
			if(outer_chunck->ck_upper == inner_chunck->ck_lower - 1)
			{
				/*Update the limits of the outer chunck*/
				outer_chunck->ck_upper = inner_chunck->ck_upper;
				
				/*remove the old chunck*/
				ll_remove(p_table.pid_ll, inner_cursor);
				kfree(inner_chunck);
				
				inner_cursor = cursor->next;
				continue;
			}
			else if(outer_chunck->ck_lower == inner_chunck->ck_upper + 1)
			{
				outer_chunck->ck_lower = inner_chunck->ck_lower;
				
				
				/*remove the old chunck*/
				ll_remove(p_table.pid_ll, inner_cursor);
				kfree(inner_chunck);
				
				inner_cursor = cursor->next;
				continue;
			}
			
			inner_cursor = inner_cursor->next;
		}
		cursor = cursor->next;
	}	
}



