/*
 * Copyright (c) 2000, 2001, 2002, 2003, 2004, 2005, 2008, 2009
 *	The President and Fellows of Harvard College.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/**
  * Contains functions that manages the file descriptors 
  */

#include <kern/unistd.h>
#include <syscall.h>
#include <lib.h>
#include <proc.h>
#include <kern/stattypes.h>
#include <kern/errno.h>
#include <filehandle.h>


 
/**
  * struct linked_list*, struct fd_chunk*, int -> struct fd_chunk*
  * EFFECTS: splits the chucnk into two chunks by the given fd
  * REQUIRES: the fd to be contained in the chunk
  * MODIFIES: struct linked_list* ll 
  * RETURNS: pointer to the splitted chunk, NULL if no chunk was created
  *
  */
static struct fd_chunk* fd_split_chucnk(struct linked_list* ll, struct fd_chunk* ck, uint32_t fd)
{
	KASSERT(fd >= ck->ck_lower && fd <= ck->ck_upper);
	KASSERT(ck != NULL);
	/*edge cases*/
	if(ck->ck_upper == fd)
	{
		ck->ck_upper--;
		return NULL;
	}
	else if(ck->ck_lower == fd)
	{
		ck->ck_lower++;
		return NULL;
	}
	
	struct fd_chunk* new_ck = fd_chunk_create(ll, ck->ck_lower, fd - 1);
	ck->ck_lower = fd + 1;

	return new_ck;
	
}

/**
  * struct fd_chunk* , int -> bool
  * EFFECTS: checks if the chunk contains the fd
  * RETURNS: true if the chunk contains the fd, false otherwise
  *
  */
static bool fd_chunk_contains(struct fd_chunk* ck, uint32_t fd)
{
	return fd >= ck->ck_lower && fd <= ck->ck_upper;
}

/**
  * struct linked_list* -> int32_t
  * EFFECTS: returns a new fd from the free list if there is any available
  * MODIFIES: struct linked_list* ll
  * RETURNS: a new fd if any was found, -1 otherwise
  * 
  */
static int32_t fd_get_from_list(struct linked_list* ll)
{
	ll_node_t* fd_node = ll_front(ll);
	if(fd_node == NULL)
	{
		return -1;
	}

	/*gets the fd and updates the chunk*/
	struct fd_chunk* ck = (struct fd_chunk*) fd_node->object;	
	uint32_t fd = ck->ck_lower;
	ck->ck_lower++;
	if(ck->ck_lower > ck->ck_upper)
	{
		ck = ll_remove(ll, fd_node);
		kfree(ck);
	}
	return fd;
}
/**************************
 *                        *
 *     public functions   *
 *                        *
 **************************/

 /**
   * struct proc* -> int
   * EFFECTS: given the proc generates a unique fd
   * RETURNS: an fd on success, or -1 if the process opened it's maximum number of files
   */
int fd_generate(struct proc* proc)
{
	KASSERT(proc != NULL);
	/*Makes sure the max number of opened files is not exceeded*/
	int n_files = cbt_get_length(proc->p_cbt_fds);
	if(n_files >= PROC_MAX_OPENED_FILES)
	{
		return -1;
	}

	int32_t fd = fd_get_from_list(proc->p_free_fds);
	/*free list is empty*/
	if(fd < 0)
	{
		int max_key = cbt_get_max_key(proc->p_cbt_fds);
		return max_key + 1;
	}
	return fd;	
}


/**
  * struct proc*, int -> void
  * EFFECTS: reclaims an fd so that it could be used later
  * REQUIRES: 1) the fd to be allocated by the generate_fd function 
  * 		  2) the file handle associated with this fd to be removed from the B-tree
  * MODIFIES: struct proc*
  */
void fd_reclaim(struct proc* proc, int fd)
{
	/*no need to reclaim*/
	int max_key = cbt_get_max_key(proc->p_cbt_fds);
	int min_key = cbt_get_min_key(proc->p_cbt_fds);
	if(fd > max_key || fd < min_key)
	{
		return;
	}
	/*Add the free list of fds*/
	struct fd_chunk* ck = fd_chunk_create(proc->p_free_fds, fd, fd);
	KASSERT(ck != NULL);
}

/**
  * struct proc*, int -> int
  * EFFECTS: requests a specific file descriptor to be marked "in use"
  * REQUIRES: the fd to be not in use by any filehandle
  * MODIFIES: proc->p_free_fds
  * RETURNS: on success returns the fd, on failure returns -1
  *
  * NOTE: if the fd you are going to use was not generated by generate_fd()
  *		  then this must be called first before using the fd
  */
int fd_request(struct proc* proc, int fd)
{
	/*looks for the fd in the list*/
	struct linked_list* ll = proc->p_free_fds;
	ll_node_t* cursor = ll->head;
	while(cursor != NULL)
	{
		/*the fd was found (removes the fd and returns)*/
		if(fd_chunk_contains((struct fd_chunk*) cursor->object, fd))
		{
			fd_split_chucnk(proc->p_free_fds, cursor->object, fd);
			return fd;	
		}
		cursor = cursor->next;
	}
	/*
     * If it is not in the free list and it 
     * lies between the min key and the max key in the list 
	 * then it must be in use
     */
	int max_key = cbt_get_max_key(proc->p_cbt_fds);
	int min_key = cbt_get_min_key(proc->p_cbt_fds);
	if(fd >= min_key && fd <= max_key)
	{
		return -1;
	}
	return fd;
}

/**
  * struct linked_list* , int, int -> struct fd_chunk*
  * EFFECTS: creates a new fd_chunk
  * REQUIRES: upper arg > lower arg
  * RETURNS: a pointer to the new chunk 
  * 
  */
struct fd_chunk* fd_chunk_create(struct linked_list* ll, uint32_t lower, uint32_t upper)
{
	struct fd_chunk* ck = kmalloc(sizeof(struct fd_chunk));
	if(ck == NULL)
	{
		return NULL;
	}
	ck->ck_lower = lower; 
	ck->ck_upper = upper;
	ll_add_front(ll, ck);
	return ck;
}

/**
  * void* -> void*
  * EFFECTS: clones the given struct fd_chunk*
  * RETURNS: pointer to the clone
  */
void* fd_chunk_clone(void* ptr)
{
	struct fd_chunk* ck = (struct fd_chunk* ) ptr;
	struct fd_chunk* clone = kmalloc(sizeof(struct fd_chunk));
	if(ck == NULL)
	{
		return NULL;
	}
	clone->ck_lower = ck->ck_lower;
	clone->ck_upper = ck->ck_upper;
	return ck;
}