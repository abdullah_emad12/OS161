/*
 * Copyright (c) 2013
 *	The President and Fellows of Harvard College.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */


/**
  * File Support for processes
  *
  * Manages the mapping between the processes file descriptors 
  * and the vnodes associated with the vfs layer.
  */
  
#include <types.h>
#include <spl.h>
#include <proc.h>
#include <current.h>
#include <vnode.h>
#include <vfs.h>
#include <kern/fcntl.h>
#include <kern/errno.h>
#include <stat.h>
#include <filehandle.h> 
/**
  * const char*, int, mode_t, struct filehandle** -> int
  * EFFECTS: Creates a file handle that will be used to read and write from the specified file
  * RETURNS: Zero on success, or an error code on FAILURE. Read the man pages for the error codes 
  *
  */
int filehandle_create(const char* filename, int flags, mode_t mode, struct filehandle** fh_ptr)
{	
	if(filename == NULL)
	{
		return EFAULT;
	}
	KASSERT(fh_ptr != NULL);

	
	struct filehandle* fh = kmalloc(sizeof(struct filehandle));
	KASSERT(fh != NULL);


	int err = vfs_open((char*)filename, flags, mode, &(fh->fh_vnode));	
	if(err)
	{
		kfree(fh);
		return err;
	}
	
	fh->fh_read = false;
	fh->fh_write = false;
	fh->fh_offset = 0;
	fh->fh_nholders = kmalloc(sizeof(int));
	*(fh->fh_nholders) = 1;
	
	/* sets rw flags according to the arg flags */

	int write = flags & O_WRONLY;
	int read =  !(flags & 0x1);
	int rw = flags & O_RDWR;
	int append = flags & O_APPEND;
	KASSERT(write || read || rw);
	if(write)
	{
		fh->fh_write = true;
	}
	else if(read)
	{
		fh->fh_read = true;
	}
	if(rw)
	{
		fh->fh_read = true;
		fh->fh_write = true;
	}
	
	/*sets the offset in case of append*/
	if(append)
	{
		struct stat f_stat;
		err = VOP_STAT(fh->fh_vnode, &f_stat);
		if(err)
		{
			vfs_close(fh->fh_vnode);
			kfree(fh);
			return err;
		}
		fh->fh_offset = f_stat.st_size;
	}
	
	
	fh->fh_rwlock = rwlock_create(filename);
	KASSERT(fh->fh_rwlock != NULL);
	
	fh->fh_global = false;
	*(fh_ptr) = fh;
	return 0;
}




/**
  * struct filehandle* -> void
  * EFFECTS: given a file handle, destroys the file handle if no process still needs it 
  * MODIFIES: struct filehandle*
  */
void filehandle_destroy(void* fh_obj)
{
	struct filehandle* fh = (struct filehandle*) fh_obj;
	struct vnode* vn = fh->fh_vnode;


	KASSERT(*(fh->fh_nholders) > 0);
	if(fh->fh_global)
	{
		return;	
	}

	*(fh->fh_nholders) = *(fh->fh_nholders) - 1;
	if(*(fh->fh_nholders) == 0)
	{
		rwlock_destroy(fh->fh_rwlock);
		kfree(fh->fh_nholders);
		kfree(fh);	
	}
	vfs_close(vn);

}

/**
  * struct filehandle* -> void
  * EFFECTS: clones the filehandle 
  * RETURN: pointer to the clone 
  */
void* filehandle_clone(void* object)
{
	struct filehandle* fh = (struct filehandle*) object;
	if(fh == NULL)
	{
		return NULL;
	}

	if(!fh->fh_global)
	{
		*(fh->fh_nholders) = *(fh->fh_nholders) + 1;
		VOP_INCREF(fh->fh_vnode);
	}
	
	return fh;
}


/**
  * bool, bool -> struct filehandle*
  * EFFECTS: creates a global filehandle with an empty vnode
  * RETURNS: pointer to the new filehandle
  * PARAMETERS: 
  * - bool read: opened for writing
  * - bool write: opened for reading
  */
struct filehandle* filehandle_create_global(bool read, bool write)
{
	struct filehandle* fh = kmalloc(sizeof(struct filehandle));
	if(fh == NULL)
	{
		return NULL;
	}
	fh->fh_read = read;
	fh->fh_write = write;
	fh->fh_vnode = NULL;
	fh->fh_offset = 0;
	fh->fh_global = true;

	fh->fh_nholders = kmalloc(sizeof(int));
	if(fh->fh_nholders == NULL)
	{
		kfree(fh);
		return NULL;
	}

	fh->fh_rwlock = rwlock_create("global");
	if(fh->fh_rwlock == NULL)
	{
		kfree(fh->fh_nholders);
		kfree(fh);
		return NULL;
	}
	return fh;
}