/*
 * Copyright (c) 2000, 2001, 2002, 2003, 2004, 2005, 2008, 2009
 *	The President and Fellows of Harvard College.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */


/**
  * File system support
  * 
  * Contains all the functions that operates on system files when a system call
  * is issued to the OS. This includes: 
  *
  * 1) open
  * 2) read
  * 3) write 
  * 4) lseek
  * 5) close
  * 6) dup2
  * 7) chdir
  * 8) __getcwd
  *
  * Refer to man or the header files for the documentations
  */



#include <kern/unistd.h>
#include <syscall.h>
#include <lib.h>
#include <proc.h>
#include <kern/fcntl.h>
#include <kern/stat.h>
#include <kern/seek.h>
#include <kern/stattypes.h>
#include <current.h>
#include <kern/errno.h>
#include <limits.h>
#include <uio.h>
#include <vnode.h>
#include <vfs.h>
#include <spl.h>
#include <filehandle.h>
#include <copyinout.h>
#include <limits.h>
/*prototypes*/




int sys_open(const char *filename, int flags, int* fd)
{
	int err;
	if(filename == NULL)
	{
		*(fd) = -1;
		return EFAULT;
	}
	char strtmp[PATH_MAX];
	size_t got;
	err = copyinstr((const_userptr_t) filename, strtmp, PATH_MAX, &got);
	if(err)
	{
		*(fd) = -1;
		return err;
	}


	struct proc* proc = curproc;
	/*creates fd*/
	*(fd) = fd_generate(proc);
	if(*(fd) == -1)
	{
		return EMFILE;
	}
	/*creates a file handle*/	
	struct filehandle* fh = NULL;	
	err = filehandle_create(filename, flags, S_IRWXU, &fh);
	if(err)
	{
		*(fd) = -1;
		return err;
	}

	cbt_insert(proc->p_cbt_fds, *(fd), fh);

	return 0;
}


int sys_read(int fd, void *buf, size_t buflen, ssize_t* ret)
{
	int err; 
	if(buf == NULL)
	{
		*(ret) = -1;
		return EFAULT;	
	}
	char tmpbuf[buflen];
	err = copyin((const_userptr_t) buf, tmpbuf, buflen);
	if(err)
	{
		*(ret) = -1; 
		return err;
	}

	/*supress warnings*/
	struct proc* proc = curproc;
	struct filehandle* fh = cbt_search(proc->p_cbt_fds, fd);
	if(fh == NULL) // no such fd
	{
		*(ret) = -1;
		return EBADF;
	}

	/*File is not opened for reading*/
	if(!fh->fh_read)
	{
		*(ret) = -1;
		return EBADF;
	}



	
	rwlock_acquire_write(fh->fh_rwlock);
	/*initialize uio*/
	struct iovec iov;
	struct uio read_uio;
	uio_kinit(&iov, &read_uio, buf, buflen, fh->fh_offset, UIO_READ);

	/*Read*/
    err = VOP_READ(fh->fh_vnode, &read_uio);

    if(err)
    {
    	*(ret) = -1;
    }
	else
	{
		fh->fh_offset = read_uio.uio_offset;
		*(ret) = buflen - read_uio.uio_resid;
	}
	rwlock_release_write(fh->fh_rwlock);

	return err;
}

int sys_write(int fd, const void *buf, size_t buflen, ssize_t* ret)
{
	int err; 
	/*checks that the buffer is valid*/
	if(buf == NULL)
	{
		*(ret) = -1;
		return EFAULT;
	}
	char tmpbuf[buflen];
	err = copyin((const_userptr_t) buf, tmpbuf, buflen);
	if(err)
	{
		*(ret) = -1; 
		return err;
	}



	struct proc* proc = curproc;
	struct filehandle* fh = cbt_search(proc->p_cbt_fds, fd);
	/*no such fd*/
	if(fh == NULL)
	{
		*(ret) = -1;
		return EBADF;
	}
	/*not opened for writing*/
	if(!fh->fh_write)
	{
		*(ret) = -1;
		return EBADF;
	}



	rwlock_acquire_write(fh->fh_rwlock);
	/*initialize uio*/
	struct iovec iov;
	struct uio write_uio;
	uio_kinit(&iov, &write_uio, (void*)buf, buflen, fh->fh_offset, UIO_WRITE);	
	
	/*Write*/
    err = VOP_WRITE(fh->fh_vnode, &write_uio);
    if(err)
    {
    	*(ret) = -1;
    }
	else
	{
		fh->fh_offset = write_uio.uio_offset;
		*(ret) = buflen - write_uio.uio_resid;
	}

	rwlock_release_write(fh->fh_rwlock);
	return err;
}

int sys_lseek(int fd, off_t pos, int whence, int32_t* ret, int32_t* ret2)
{
	/*gets the filehandle*/
	struct proc* proc = curproc;
	struct filehandle* fh = cbt_search(proc->p_cbt_fds, fd);
	/*no such fd*/
	if(fh == NULL)
	{
		*(ret) = -1;
		*(ret2) = -1;
		return EBADF;
	}

	/*fd points to a device that does not support seeking*/
	if(!VOP_ISSEEKABLE(fh->fh_vnode))
	{
		*(ret) = -1;
		*(ret2) = -1;
		return ESPIPE;
	}
	
	/*updates the offset according to the whence*/
	off_t new_off = 0;
	switch(whence)
	{
		case SEEK_SET:
			new_off = pos;
			break;
		case SEEK_CUR:
			new_off = fh->fh_offset + pos;
			break;
		case SEEK_END:
		{
			struct stat f_stat;
			int err = VOP_STAT(fh->fh_vnode, &f_stat);
			if(err)
			{
				*(ret) = -1;
				*(ret2) = -1;
				return ESPIPE;
			}
			
			new_off = f_stat.st_size + pos;
			break;
		}
		default: 
			return EINVAL;
			break;
	}
	
	
	if(new_off < 0)
	{
		*(ret) = -1;
		*(ret2) = -1;
		return EINVAL;
	}
	/*updates the offset*/
	rwlock_acquire_write(fh->fh_rwlock);
	fh->fh_offset = new_off;
	rwlock_release_write(fh->fh_rwlock);
	*(ret2) = new_off;
	*(ret) = new_off >> 32;
	
	return 0;
}

int sys_close(int fd, int* ret)
{
	struct proc* proc = curproc;
	struct filehandle* fh = (struct filehandle*) cbt_delete(proc->p_cbt_fds, fd);
	/*fd is not valid*/
	if(fh == NULL)
	{
		*(ret) = -1;
		return EBADF;
	}
	fd_reclaim(proc, fd);
	filehandle_destroy(fh);
	return 0;
}

int sys_dup2(int oldfd, int newfd, int* ret)
{
	struct proc* proc = curproc;
	
	/*invalid newfd*/
	if(newfd < 0)
	{
		*(ret) = -1;
		return EBADF;
	}
	

	/*checks the limit on the max no of opened files*/
	int n_files = cbt_get_length(proc->p_cbt_fds);
	if(n_files >= OPEN_MAX || newfd >= OPEN_MAX)
	{
		*(ret) = -1;
		return EBADF;
	}

	/*if the newfd is the same as the old*/
	if(newfd == oldfd)
	{
		*(ret) = newfd;
		return 0;
	}

	/*clone the old file handle if it exists*/
	struct filehandle* fh = cbt_search(proc->p_cbt_fds, oldfd);
	if(fh == NULL)
	{
		*(ret) = -1;
		return EBADF;
	}
 	rwlock_acquire_write(fh->fh_rwlock);
	struct filehandle* newfh = filehandle_clone(fh); 
	if(newfh == NULL)
	{
		rwlock_release_write(fh->fh_rwlock);
		*(ret) = -1;
		return EBADF;
	}

	

	
	int old_max = cbt_get_max_key(proc->p_cbt_fds);
	
	/*closes the filehandle of the newfd if any*/
	int tmp;
	sys_close(newfd, &tmp);
	
	newfd = fd_request(proc, newfd);
	KASSERT(newfd >= 0);

	cbt_insert(proc->p_cbt_fds, newfd, newfh);

	int new_max = cbt_get_max_key(proc->p_cbt_fds);

	/*inserts a list of free fds*/
	if(new_max > old_max)
	{
	 	fd_chunk_create(proc->p_free_fds, old_max + 1, new_max - 1);			
	}
	rwlock_release_write(fh->fh_rwlock);
	
	*(ret) = newfd;
	return 0;
}


int sys_chdir(const char *pathname)
{
	int err;
	if(pathname == NULL)
	{
		return EFAULT;
	}
	char strtmp[PATH_MAX];
	size_t got;
	err = copyinstr((const_userptr_t) pathname, strtmp, PATH_MAX, &got);
	if(err)
	{
		return err;
	}

	int spl = splhigh();
	struct proc* proc = curproc;

	if(pathname == NULL)
	{
		splx(spl);
		return EFAULT;
	}	

	/*attempts to open the new directory*/
	struct vnode* newDir; 
	err = VOP_LOOKUP(proc->p_cwd, kstrdup(pathname), &newDir);
	if(err)
	{
		splx(spl);
		return err;
	}

	/*makes sure this is a directory*/
	mode_t file_type;
	err = VOP_GETTYPE(newDir, &file_type); 
	if(err)
	{
 		vfs_close(newDir);
		splx(spl);
		return err;
	}
	if(file_type != _S_IFDIR)
	{
		vfs_close(newDir);
		splx(spl);
		return ENOTDIR;
	}
	vfs_close(proc->p_cwd);
	proc->p_cwd = newDir;
	splx(spl);
	return 0;
}


int sys___getcwd(char *buf, size_t buflen, int* ret)
{
	int err;
	if(buf == NULL)
	{
		return EFAULT;
	}
	char strtmp[buflen];
	err = copyin((const_userptr_t) buf, strtmp, buflen);
	if(err)
	{
		return err;
	}

	int spl = splhigh();
	struct proc* proc = curproc;
	if(buf == NULL)
	{
		*(ret) = -1;
		splx(spl); 
		return EFAULT;
	}

	struct iovec iov;
	struct uio read_uio;
	uio_kinit(&iov, &read_uio, buf, buflen, 0, UIO_READ);
	
	err = VOP_NAMEFILE(proc->p_cwd, &read_uio);
	if(err)
	{
		*(ret) = -1;
		splx(spl);
		return err;
	}

	*(ret) = buflen - read_uio.uio_resid;
	splx(spl); 
	return 0;
}

int sys_mkdir(const char *pathname, mode_t mode)
{
	struct proc* proc = curproc;
	if(pathname == NULL)
	{
		return EFAULT;
	}


	int err = VOP_MKDIR(proc->p_cwd, pathname, mode);
	if(err)
	{
		return err;
	}

	return 0;
}

int sys_rmdir(const char *pathname)
{
	struct proc* proc = curproc;
	if(pathname == NULL)
	{
		return EFAULT;
	}


	int err = VOP_RMDIR(proc->p_cwd, pathname);
	if(err)
	{
		return err;
	}

	return 0;
}

int sys_remove(const char* path, int* ret)
{
	*(ret) = 0;
	int err;
	if(path == NULL)
	{
		*(ret) = -1;
		return EFAULT;
	}
	char strtmp[PATH_MAX];
	size_t got;
	err = copyinstr((const_userptr_t) path, strtmp, PATH_MAX, &got);
	if(err)
	{
		*(ret) = -1;
		return err;
	}


	err = vfs_remove((char*)path);
	if(err)
	{
		*(ret) = -1;
	}
	return err;
}
/*****************************
 *                           *
 *        Helpers            *
 *                           *
 *****************************/

