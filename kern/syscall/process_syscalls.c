/*
 * Copyright (c) 2000, 2001, 2002, 2003, 2004, 2005, 2008, 2009
 *	The President and Fellows of Harvard College.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */


/**
  * Process support
  * 
  * Contains all the functions that operates on processes when a system call
  * is issued to the OS. This includes: 
  *
  * 1) getpid
  * 2) fork
  * 3) execv 
  * 4) waitpid
  * 5) _exit
  *
  * Refer to man or the header files for the documentations
  */


#include <kern/unistd.h>
#include <syscall.h>
#include <lib.h>
#include <proc.h>
#include <current.h>
#include <kern/errno.h>
#include <kern/wait.h>
#include <kern/fcntl.h>
#include <addrspace.h>
#include <mips/trapframe.h>
#include <synch.h>
#include <thread.h>
#include <copyinout.h>
#include <filehandle.h>
#include <vfs.h>
#include <limits.h>
struct process_table p_table;

/*prototypes*/
static int argv_copyin(char** argv, char*** copy_ptr);
static int argv_count(char** argv);
static int argv_copyout(paddr_t* stack_ptr, char** argv);
static void argv_cleanup(char** argv);
static int check_pointer(char** ptr);


pid_t sys_getpid(void)
{
	struct proc* proc = curproc;
	return proc->p_id;
}
void sys__exit(int exitcode, int exitState)
{
	struct proc* proc = curproc;
	lock_acquire(proc->p_cv_lock);

	/*encode the the exit code*/
	switch(exitState)
	{
		case __WCORED:
			exitcode = _MKWAIT_CORE(exitcode);
		break;
		case __WSIGNALED:
			exitcode = _MKWAIT_SIG(exitcode);
		break;
		case __WSTOPPED:
			 exitcode = _MKWAIT_STOP(exitcode);
		break;
		default:
			 exitcode = _MKWAIT_EXIT(exitcode);
		break;
	}
	proc->p_exit_status = exitcode;




	proc->p_terminated = true;
	cv_broadcast(proc->p_cv_waitpid, proc->p_cv_lock);
	
	/*destroys all the terminated children*/
	for(ll_node_t* cursor = ll_front(proc->p_ll_children); cursor != NULL; cursor = cursor->next)
	{
		struct proc* child = (struct proc*) cursor->object;
		lock_acquire(child->p_cv_lock);
		if(child->p_terminated)
		{
			lock_release(child->p_cv_lock);
			proc_destroy(child);
		}
		else
		{
			child->p_parent = NULL;	
			lock_release(child->p_cv_lock);
		}
	}
	/*
     * detach adress space here becuase we 
     * are not sure whether proc_destroy is going
     * to be called here or not
     */
	struct addrspace* as = proc_setas(NULL);
	as_deactivate();
	as_destroy(as);
	
	lock_release(proc->p_cv_lock);
	proc_remthread(curthread);
	if(proc->p_parent == NULL)
	{
		proc_destroy(proc);
	}	

	thread_exit();


}

int sys_waitpid(pid_t pid, userptr_t status, int options, pid_t* ret)
{
	int err;
	/*makes sure status is NULL or a valid pointer*/
	if(status != NULL)
	{
		int tmp;
		err = copyin(status, &tmp, sizeof(int));
		if(err)
		{
			*(ret) = -1;
			return err;
		}
	}

	/*the only supported options are none or WHOHANG*/
	if(options != 0 && options != WNOHANG)
	{
		*(ret) = -1;
		return EINVAL;
	}
	struct proc* proc = curproc;
	struct proc* childproc = cbt_search(p_table.p_cbt, pid);
	/*nonexistent proc*/
	if(childproc == NULL)
	{
		*(ret) = -1;
		return ESRCH;
	}
	/*it's not one of its children*/	
	if(ll_find(proc->p_ll_children, childproc) == NULL)
	{
		*(ret) = -1;
		return ECHILD;
	}
	lock_acquire(childproc->p_cv_lock);
	while(!options && !childproc->p_terminated)
	{
		cv_wait(childproc->p_cv_waitpid, childproc->p_cv_lock);
	}
	/*WHOWANG option was passed*/
	if(!childproc->p_terminated)
	{
		*(ret) = 0;
		return 0;
	}

	/*collect status*/
	int status_ret = childproc->p_exit_status;
	int pid_ret = childproc->p_id;
	
	/*remove from the list of children and destroy*/	
	ll_node_t* removed_node = ll_find(proc->p_ll_children, childproc);
	KASSERT(removed_node != NULL);

	void* object = ll_remove(proc->p_ll_children, removed_node);
	KASSERT(object != NULL);
	
	lock_release(childproc->p_cv_lock);

	proc_destroy(childproc);
	if(status != NULL)
	{
		copyout(&status_ret, status, sizeof(int));
	}
	*(ret) = pid_ret;
	return 0;
}


int sys_fork(struct trapframe* tf, pid_t* pid)
{	
	
	/*the system has too many processes running*/
	if(cbt_get_length(p_table.p_cbt) >= PROC_LIMIT)
	{
		*(pid) = -1;
		return ENPROC;
	}

	/*The current process has forked many processes*/
	struct proc* parentproc = curproc;
	if(ll_length(parentproc->p_ll_children) >= PROC_CHILD_LIMIT)
	{
		*(pid) = -1;
		 return EMPROC;
	}

	/*copy the contents of the trap frame*/	
	struct trapframe* clone_tf = kmalloc(sizeof(struct trapframe));
	if(clone_tf == NULL)
	{
		*(pid) = -1;
		return 	ENOMEM;
	}
	memcpy(clone_tf, tf, sizeof(struct trapframe));

	
	/*create new process*/
	struct proc* childproc = proc_create_runprogram(parentproc->p_name);
	if(childproc == NULL)
	{
		kfree(clone_tf);
		*(pid) = -1;
		return ENOMEM;
	}


	/*copy address space*/
	struct addrspace* as = NULL; 
	int err =  as_copy(parentproc->p_addrspace, &as);
	if(as == NULL)
	{
		kfree(clone_tf);
		proc_destroy(childproc);
		*(pid) = -1;
		return err;
	}
	if(err)
	{
		kfree(clone_tf);
		proc_destroy(childproc);
		*(pid) = -1;
		return err;
	}
	childproc->p_addrspace = as;
	
	/* removes the filehandles tree and clones the parent's instead*/	
	cbt_destroy(childproc->p_cbt_fds, NULL);

	struct cranbtree* cbt = cbt_clone(parentproc->p_cbt_fds, filehandle_clone);
	if(cbt == NULL)
	{
		kfree(clone_tf);
		proc_destroy(childproc);
		*(pid) = -1;
		return err;
	}
	cbt_detach_clone(cbt); /*necessary so that the cranbtree library does
							 not treat this cranbtree as a clone.*/
	childproc->p_cbt_fds = cbt;
	


	struct linked_list* ll = ll_clone(parentproc->p_free_fds, fd_chunk_clone);
	if(ll == NULL)
	{
		kfree(clone_tf);
		proc_destroy(childproc);
		*(pid) = -1;
		return err;
	}
	childproc->p_free_fds = ll;

	/*sets the parent and children fields*/
	childproc->p_parent = parentproc;
	ll_add(parentproc->p_ll_children, childproc);

    as_activate();

	/*clones*/
	err = thread_fork(childproc->p_name,
	    			  childproc,
	   				  &fork_entrypoint,
	    			  (void*)clone_tf, 0);

	if(err)
	{
		kfree(clone_tf);
		proc_destroy(childproc);
		*(pid) = -1;
		return err;
	}
	
	*(pid) = childproc->p_id;
	return 0;
}


void fork_entrypoint(void* obj, long unsigned junk)
{
	(void) junk;
	struct trapframe* tf = (struct trapframe*) obj;
	enter_forked_process(tf);

}

int sys_execv(const char* progname, char** argv)
{
	
	/*variables*/
	struct addrspace* as;
	struct vnode* prog_vnode;
	vaddr_t entrypoint, stackptr;
	int err;
	struct proc* proc = curproc;
	int argc; 
	char* progname_kern;
	

	/*Validates the progrname and moves it in the kernel space*/
	if(progname == NULL)
	{
		return EFAULT;
	}
	char strtmp[PATH_MAX];
	size_t got;
	err = copyinstr((const_userptr_t) progname, strtmp, PATH_MAX, &got);
	if(err)
	{
		return err;
	}
	if(strlen(strtmp) == 0)
	{
		return EISDIR;
	}
	progname_kern = kmalloc(sizeof(char) * got);
	if(progname_kern == NULL)
	{
		return ENOMEM;
	}	
	strcpy(progname_kern, strtmp);
	

	/*validates the argv*/
	err = check_pointer(argv);
	if(err)
	{
		kfree(progname_kern);
		return err;
	}
	
	/*Copies the arguments to the kernel space first*/
	argc = argv_count(argv);
	char** argv_kern;
	err = argv_copyin(argv, &argv_kern);
	if(err)
	{
		kfree(progname_kern);
		return err;	
	}


	err = vfs_open((char*)progname, O_RDONLY, 0, &prog_vnode);
	if(err)
	{
		kfree(progname_kern);
		argv_cleanup(argv_kern);
		return err;
	}
	
	/*there should be an existing address space */
	KASSERT(proc_getas() != NULL);
	
	/*deactivate the old address space*/
	as_deactivate();


	/*creates new address space*/
	as = as_create();
	if (as == NULL) 
	{
		kfree(progname_kern);
		argv_cleanup(argv_kern);
		vfs_close(prog_vnode);
		return ENOMEM;
	}
	/* Switch to it and activate it. */
	struct addrspace* old_as = proc_setas(as);
	as_activate();

	err  = load_elf(prog_vnode, &entrypoint);
	vfs_close(prog_vnode);
	if(err)
	{
		kfree(progname_kern);
		argv_cleanup(argv_kern);
		as_deactivate();
		as_destroy(as);
		proc_setas(old_as);
		as_activate();	
		return err;
	}

	err = as_define_stack(as, &stackptr);
	if(err)
	{
		kfree(progname_kern);
		argv_cleanup(argv_kern);
		as_deactivate();
		as_destroy(as);
		proc_setas(old_as);
		as_activate();	
		return err;
	}
	

	
	/*copy args to the userspace*/
	err = argv_copyout(&stackptr, argv_kern);
	if(err)
	{
		kfree(progname_kern);
		argv_cleanup(argv_kern);
		as_deactivate();
		as_destroy(as);
		proc_setas(old_as);
		as_activate();	
		return err;
	}

	    
    vaddr_t baseAddress = USERSTACK;
    vaddr_t argvPtr = stackptr;
    vaddr_t offset = ROUNDUP(USERSTACK - stackptr,8);
    stackptr = baseAddress - offset;



	argv_cleanup(argv_kern);
	kfree(proc->p_name);
	proc->p_name = progname_kern;
	as_destroy(old_as);	
	argc--; /*becuase this counted the NULL argument as well*/
	enter_new_process(argc, (userptr_t) argvPtr, NULL, stackptr, entrypoint);

	return 0;
}



/******************
 *     helpers    *
 ******************/

/**
  * char** -> int
  * EFFECTS: Counts the number of the argv arguments
  * RETURNS: 0 if all the memory is valid, error code otherwise
  */
static int argv_count(char** argv)
{
	int i;
	for(i = 0; argv[i] != NULL; i++);
	return i + 1;
}

/**
  * char**, char** -> int
  * EFFECTS: copies the arguments vector from the userspcae to the kernel space
  * MODIFIES: char*** copy_ptr
  * RETURNS: 0 on success, error code otherwise
  */ 
static int argv_copyin(char** argv, char*** copy_ptr)
{

	int err;
	size_t count = 0; /*must be set to -1 if the count exceeds ARG_MAX*/
	int length = argv_count(argv);
	char** copy;
	KASSERT(length > 0);
	copy = kmalloc(sizeof(char*) * length);
	memset(copy, 0, length);
	if(copy == NULL)
	{
		return ENOMEM;
	}


	/**
      * Allocates a big char* chunck that will be 
      * treated as an array of strings where each two
	  * consuctive strings in the array are seperated by 
      * the NULL terminator. The address of the first character in each
      * string in argv_kern is then copied to char** copy for fast reference
	  * Which is then treated as an array of strings when origianlly its only one big string
	  * that contains small strings seperated by NULL terminator
      */
	char* argv_kern = kmalloc(sizeof(char) * ARG_MAX); 
	char* argv_cursor = argv_kern;
	*(copy) = argv_kern;

	for(int i = 0; argv[i] != NULL; i++)
	{
		/*validate the pointer*/
        err = check_pointer(&argv[i]);
		if(err)
		{
			argv_cleanup(copy);
			*(copy_ptr) = NULL;
			return err;
		}
		/*copy string from the userspace*/
		size_t max_len = ARG_MAX - count;		
		size_t got;
		err = copyinstr((const_userptr_t) argv[i], argv_cursor, max_len,  &got);
		if(err)
		{
			argv_cleanup(copy);
			*(copy_ptr) = NULL;
			return err;			
		}				

		count += got;
        /*exceeds the arg max*/
		if(count > ARG_MAX)
		{
			argv_cleanup(copy);
			*(copy_ptr) = NULL;
			return E2BIG;				
		}
		copy[i] = argv_cursor;
		argv_cursor += sizeof(char) * got;
	}
	
	copy[length-1] = NULL;
	
	/*success*/	
	*(copy_ptr) = copy;
	return 0;
}

/**
  * paddr_t*, char** -> int
  * EFFECTS: copies the arguments content to the user stack, 
  *			 then copies the the argc and argv pointers to the stack.
  *			 Does all the work for copying the arguments to the new process
  * 		 created by execv
  * MODIFIES: paddr_t* -> sets it to the new stack pointer
  * RETURNS: 0 on success, or error code on failure	
  */
static int argv_copyout(vaddr_t* stack_ptr, char** argv)
{
	int err;
	userptr_t stack = (userptr_t) *(stack_ptr);
	int argc = argv_count(argv);
	userptr_t* argv_usrptr = kmalloc(sizeof(argv_usrptr) * argc);	
	int i;

	/*copy the content of argv to the userstack*/
	for(i = 0; argv[i] != NULL; i++)
	{
		int len = strlen(argv[i]) + 1;
		len = ROUNDUP(len, 4);
		stack -= sizeof(char) * len;
		size_t got;
		err = copyoutstr(argv[i], stack, len, &got);
		if(err)
		{
			kfree(argv_usrptr);
			return err;
		}

		argv_usrptr[i] = stack;
	}
	argv_usrptr[i] = NULL;

	/*copies the pointers of argv (last location is NULL)*/
	for(i = argc-1; i >= 0; i--)
	{	
		stack -= sizeof(paddr_t);
		err = copyout(&argv_usrptr[i], stack, sizeof(paddr_t));
		if(err)
		{
			kfree(argv_usrptr);
			return err;
		}
	}

	
	


	/*success*/
	kfree(argv_usrptr);
	*(stack_ptr) = (paddr_t) stack;
	return 0;
}

/** 
  * char** -> void
  * EFFECTS: frees all the memory associated with argv
  * MODIFIES: argv
  */
static void argv_cleanup(char** argv)
{
	kfree(*(argv));
	kfree(argv);
}

/**
  * char** -> int
  * EFFECTS: makes sure the pointer is valid 
  * RETURNS: 0 if the pointer is valid, error code otherwise
  */
static int check_pointer(char** ptr)
{
	/*the arguments vector cant be NULL*/
	if(ptr == NULL)
	{
		return EFAULT;
	}

	const_userptr_t tmp_arg = (const_userptr_t) ptr;
	char** tmp_arg_test = NULL;
	int err = copyin(tmp_arg, &tmp_arg_test, sizeof(char**));
	if(err)
	{
		return EFAULT;
	}
	return 0;

}
