# System Calls
This document contains information about the system call implementation. The current set of system calls supported in this version of the Cranberry OS can be divided into two subsets; namely, file system system and process system calls. 

**Disclaimer:**
This document discusses the internel design of the system call implementation. However, it does not discuss the interface and many details such as error codes will be left out. Such details can be found in the [man pages](http://htmlpreview.github.io/?https://gitlab.com/abdullah_emad12/OS161/blob/master/man/index.html) of this repository or through the Unix manual pages.

## Supported System calls: 
<table>
1. open 
2. read
3. write
4. lseek
5. close 
6. dup2
7. chdir
8. __getcwd
9. getpid
10. fork 
11. execv
12. waitpid
13. _exit
</table>

## File system call

The file system calls supported in the Cranberry operating system are open , read , write , lseek , close , dup2 , chdir , and __getcwd. before diving into the specific implementation details let us first talk a look at common data structures and design patterns and examine how are they integrated in the process data structure.


#### Processes

Every process has a current working directory of which the relative paths are calculated. Additionally, every process has a list of mappings i.e: fdv_map which maintains the file descriptors associated with the specific process and the vnode of the opened file. Whenever, a new process is initialized two virtual files that points to the 

 
#### filehandle

A file handle is a high level struct used by the syscalls to identifiy a file opened by a certain user. It contains important such as the offset of the file associated with the holding process. A file handle contains file an rw lock that must be acquired on every read or write.

#### Mappings List

In order to boost the performance, a B-tree is used to store the filehandle where each node is identified by the file descriptor.

#### fd Generation
There is no restriction on the value of the file descriptor other than it must be a positive 32 bits value. The approach taken to generate file descriptors is simple fairly simple but first, we must discuss some structure used to manage file descriptors. The struct fd_chunk, which can be found in kern/include/proc.h, is used to represent a segment of free file descriptors by an upper and lower bounds.Specifically, every number between the lower and upper bound inclusive is a free descriptor that can be used to indentify a filehandle. Moreover, every proc has a linkedlist of fd_chunks, that is initially empty. Our goal now when managing the file descriptors is to preserve the following invariants: 
- All the numbers n, where n >= 0, greater then the maximum filedescriptors and less then the minimnum file descriptors in the filehandle B-tree are available numbers that can be used in the future to point to filehandles. (You can easily convince yourself why this should trivially always hold).
- All the numbers betweeen the maximum and the minimum filedescriptors in the B-tree must be either in use or available and can be found in chuncks in the linked list of free desriptors. Our main goal is make sure that there is no in between, that is there is no filedescriptor that is available but does not exist in a chunk in the list.

our apraoch for generation is simple: 
1. return and remove the first element in the first chunk of the first node in the linked list in case the the list was not empty.
2. otherwise return (the maximum key in the B-tree + 1)

To reclaim a fd you simple: 
1. do nothing if the filedescriptor is less then the minimum key in the tree or greater than the maxmimum in the tree.
2. Add a new chunk with a lower and upper values equal to the file descriptor and prepend to the linked list


To request a specific file descriptor: 
1. Remove the file descriptor in the free list if it is found


#### sys_open

This function gets called when the open system call is issued. First it will choose a file descriptor simply by getting the last fdv_map in the last mapping list (element with maximum fd) and increment on it's file descriptor. Then a fdv_map is then created according to the rule mentioned above in the fdv_map section and inserted inside the list to the appropriate position. If any error occurs in the middle of the opening the file and inserting it in the list the appropriate error code is returned to the user. Read the Unix manual for more information. Only 1024 files and devices are allowed to opened by a process at a time.


#### sys_read

Gets called when the read system call is issued. It will search in the list for the appropriate node specified by the file descriptor. If no such a node is found, -1 is returned and errorno is set to the appropriate error code. Otherwise it will check if the node has a read uio and will read the appropriate data according to offset. 

#### sys_write

Gets called when the write system call is issued. It does the same thing as sys_read but writes instead of reading.


#### sys_lseek

Gets called when the lseek system call is issued. It looks for the node specified by the file descriptor and changes the offset of the uio according to the provided offset and the position passed to the function.


#### sys_close

Gets called when the lseek system call is issued. Closes the file using vfs_close() and destroys the mapping after removing it from the list.

#### sys_dup2

Gets called when the dup2 system call is issued.

1) Search the list for the file handle specified by the old fd and clones a it.
2) Closes the file described by the new file descriptor. 
3) Fix the free fds list 


#### sys_chdir

Gets called when the chdir system call is issued. Makes sure the given path already existed (returns an error code if it doesnot) and changes the path of the process.

#### sys_getpid

This is the easiest syscall of all. It never fails and it just returns the process id of the caller.


#### Fork 
Forks work by copying the address space of the calling process to a new address space. Every thing in the new process is a copy of the old process except for the filehandles which are not copied but rather shared between the parent and the child.

#### waitpid / exit
Waitpid will only work in the case where the process to be waited for is one of the calling processes's children. It will first check to make sure the child has not exited yet and is still running, if it is, sys_waitpid will put the current thread to sleep and wait for the child to signal it. Otherwise, it will collect the exit code and status from the child proc and return it to the calling process. However, there is a subtlety here, who is responsible for destroying the proc struct? In cranberry OS, every child must keep track of his parent process and every parent must keep track of its children. Every child is allowed to have only one parent but every parent is allowed to have as many children as allowed. After collecting the infromation from the  child the parent will destroy the child process.

On the other hand sys_exit will signal to all the processes waiting for this process to exit. It will then iterate through its children and check wether they have exited or not, if they have, it will destroy their procs. If they have not, their parent field will be set to NULL. Finally it will check its parent field, if it's NULL, the process will destroy itself, otherwise will exit and leave the parent process to destroy it.


#### execv 
This system call replaces the current addresspace with the content of other program. Execv passes the arguments to the new program through copying them all in the kernel space first, then putting them on the userstack afterward. Note that the arguments are placed contiguously in the kernel space to avoid segmentation of memory.  