# Process 

This document contains information about the processes in the kernel, how they are managed and special design decisions releated to them.


#### Process structure

Any newly created Process will contain the following attributes which defines it: 

1. Process Name: the name of the created process
2. process ID: Unique ID, allocated by the process manager
3. number of threads. 
4. The address space. i.e: Code, Heap, stack
5. current working directory.
6. list of file descriptors opened by the process.

#### Managing the process ID 

Every process must have a unique ID that identifies it. No two running processes can have the same process id at the same time. In this section we will discuss the strategy that is used to manage the process IDs and ensure that previously used ids can be recycled. 

When the kernel bootsup it initalizes the process table which contains all the running processes as well as a structure for managing free process IDs. The structure is basically a linked list of free pid chuncks. A chunck has an upper and lower limit where all the numbers between the lower and the upper limits are free for allocation. Example: a chunck with lower limit = 5 and upper limit = 20 means that pids 5, 6, 7, 8, ...., 20 are available for allocation. Initially the list contains one chunck from 2 to 32768. When a new process is created, it will request a free process ID. The manager will look at the first chunck in the list and grabs the first pid then increment the lower limit. If all the IDs in the chunck were consumed, the chunck node will be removed from the list.

After a process is terminated, the PID must be freed to be reused by other new processes. The manager will take the freed PID and search through the list of chuncks and modifies the limits of the chunck if it can be added Otherwise add a new chunck to the list.
Example: Assume the returned pid was 15 and we have the following pid chuncks:
- 5 -> 11
- 16 -> 25
- 30 -> 50
The function will search through the list will find the chunck containing pid 16 to 25 and will decrement 16 to include 15 in the chunck. If it was not possible to add 16 to the list, the manager will simply add a new chunck from 16 to 16. If the size of the list reached a certain threshold define in the proc.h header file, the function will attempt to combine together different chuncks into one chunck.