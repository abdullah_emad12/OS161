#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
int main(int argc, char* argv[])
{

	printf("Forking my self\n");
	const char* childstr = "I am the child process\n\n";
	printf("The number of arguments is: %d and they are: \n", argc);
	for(int i = 0; i < argc; i++)
		printf("%s\n", argv[i]);
	printf("\n\n");
	if(argv[argc] == NULL)
	{
		printf("correct\n");
	}

	pid_t pid = fork();
	if(pid == 0)
	{
		write(1, childstr, strlen(childstr));	
	}
	else
	{	
		const char* str = "I am the parent process and my child has pid: 3\n";
		write(1, str, strlen(str));
	}
	exit(0);
}
